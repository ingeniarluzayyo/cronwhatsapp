<?php

$contact= new Contact();
if(!$contact->IsAnyInProgress())
{
	$contact->PutInProgress();
	$contacts = $contact->GetAllInProgress();

	$user_admin = 1;

	if(count($contacts) > 0)
	{
		$sender = new Sender();
		$sender = $sender->GetSenderAvailableToSyncronize($user_admin);

		if($sender->id != 0)
		{
			$u = $contacts;

			if(!is_array($u))
			{
				$u = array($u);
			}
			
			$numbers = array();
			foreach($u as $number)
			{
				if(substr($number, 0, 1) != "+")
				{
				//add leading +
				$number = "+$number";
				}
				$numbers[] = $number;
			}
			
			try{
				$wa = new WhatsProt($sender->numero, "WhatsApp", false);
				
				//bind event handler
				$wa->eventManager()->bind('onGetSyncResult', 'onSyncResult');
				
				$wa->connect();
				$wa->loginWithPassword($sender->password);
				
				//send dataset to server
				$wa->sendSync($numbers);
				//wait for response
				while(true)
				{
					$wa->pollMessage();
				}
			}catch(LoginFailureException $e_l){
				echo "[Whatsapp install]Login Faild Error <br />";
				$sender->habilitado = 0;
				$sender->Update_available();
			}catch(Exception $ex){
				//Solo para salir del ciclo
			}
			
			$sender->assigned_user = 0;
			$sender->AssignToUser();	
			$contact->PutInNotYetVerified();	
		}else{
			echo "[Whatsapp install] No sender available.";
		}			
	}
}else{
	echo "Another WhatsApp Install In progress<br/>";
}
//event handler
/**
* @param $result SyncResult
*/
function onSyncResult($result)
{
	global $contact;
	global $sender;
	
	foreach($result->existing as $number)
	{
		$number = explode('@',$number);
		echo "<span style='color:green;'>".$number[0]."</span>";
		$contact->Modificar_Whatsapp_Install(trim($number[0]),"Yes");
	}
	
	foreach($result->nonExisting as $number)
	{
		echo "$number does not exist<br />";
		$contact->Modificar_Whatsapp_Install($number,"No");
	}
	
	$sender->assigned_user = 0;
	$sender->AssignToUser();
	throw new Exception();//to break out of the while(true) loop
}

?>