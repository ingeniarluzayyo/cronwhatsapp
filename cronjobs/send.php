<?php
$campaign = new Campaign();
$bulk_message = new Bulk_Messages();
$sender = new Sender();
$group = new Group();

if($campaign->GetFirstQueue())
{
	$tiempo_inicio = microtime_float();//Benchmark
	
	$campaign->status = 'in progress';
	$campaign->Update_Status();
	
	$bulk_message->campaign_id = $campaign->id;
	$bulk_messages = $bulk_message->GetAllOfCampaign();
	
	$group->GetById($campaign->group_id);
	$user = $group->id_user;
	
	$finish = true;

	foreach($bulk_messages as $m)
	{			
			if(!$settings->TimeToSend())
			{
				echo "Its not the sending time.<br>";
				$campaign->status = 'waiting';
				$campaign->Update_Status();
				$finish = false;
				break;
			}
			
			$campaign->GetById($campaign->id);
			
			if($campaign->status == 'paused'){
				echo "The campaign was paused.<br>";
				$finish = false;
				break;
			}

			if(!$sender->HaveEnougthSenders($user))
			{
				echo "Not enougth senders to send.<br>";
				$campaign->status = 'waiting';
				$campaign->Update_Status();
				$finish = false;
				break;
			}
		
			
			$contact = new Contact();
			$contact->GetById($m->contact_id,$user);	
			
			echo("Starting send [:".$contact->number."]<br>");							
		    $sender_id = $m->Send($user,$contact,$campaign->type,$campaign->content);	    
			if($sender_id > 0)
			{
				echo("Sent to [:".$contact->number."]<br><br>");
				$m->sent = 1;
			}else{
				echo("Error sending to [".$contact->number."]<br /><br />");
				$m->sent = -1;
			}
			
			$m->sender_id = $sender_id;
			$m->Update_sent();
			$m->Update_sender();
			$m->Update_date();
			
			sleep(SLEEP_TIME_BETWEEN_MSG);		
	}
	
	if($finish)
	{
		$campaign->status = 'finish';
		$campaign->Update_Status();
	}
	
	$tiempo_fin = microtime_float();
	$tiempo = $tiempo_fin - $tiempo_inicio;
		
	echo "Tiempo empleado: " . ($tiempo_fin - $tiempo_inicio); 
}else{
	echo "No campaigns.";
}

function microtime_float()
{
	list($useg, $seg) = explode(" ", microtime());
	return ((float)$useg + (float)$seg);
}
?>