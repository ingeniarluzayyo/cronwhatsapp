<?php
require_once(__DIR__.'/../lib/variables.php');
require_once(__DIR__.'/../lib/clases.php');
//Modelos
require_once(__DIR__.'/../models/ErrorLog.class.php');
require_once(__DIR__.'/../lib/api/whatsapp/whatsprot.class.php');
require_once(__DIR__.'/../models/Usuario.class.php');
require_once(__DIR__.'/../models/HashMedia.class.php');
require_once(__DIR__.'/../models/Sender.class.php');	
require_once(__DIR__.'/../models/Bulk_Messages.class.php');	
require_once(__DIR__.'/../models/Single_Messages.class.php');
require_once(__DIR__.'/../models/Contact.class.php');
require_once(__DIR__.'/../models/Campaign.class.php');
require_once(__DIR__.'/../models/Group.class.php');
require_once(__DIR__.'/../models/ListenSent.class.php');

//Error Log
$error_log = new ErrorLog();	

//Settings//Control de rango de hora
$dt_now = new DateTime();
define('SEND_START_HOUR',$settings->hora_inicio);
define('SEND_END_HOUR',$settings->hora_fin);
define('SLEEP_TIME_BETWEEN_MSG',$settings->sleep_time_between_msg);

//Cronjobs
require_once(__DIR__.'/clean.php');

//Sending Hour control
if($settings->TimeToSend())
{
	require_once(__DIR__.'/send.php');
}

require_once(__DIR__.'/whatsapp_install.php');

if($settings->IsOnlineTime())
{
	require_once(__DIR__.'/is_online.php');
}

if($settings->ListenTime())
{
	require_once(__DIR__.'/listen.php');
}
?>