<?php

	class ErrorLog {
		
		public $id;
		public $user;
		public $contact;
		public $contenido;
		public $type;
		public $fecha;
		public $error_type;
		public $sender;
		public $exception;
		public $tabla_error_envio;		
		
		
		public function __construct() {
			$this->tabla_error_envio = 'error_send';
		}
		
		public function setTabla($tabla)
		{
			$this->tabla_error_envio = $tabla;
		}
		
		public function LogErrorSend($id_user,$contact,$contenido,$type,$error_type,$sender = "",$exception = "") {
			$dt = new DateTime();
			$result = true;
									
			$arr = array(
				'user' 			=> $id_user,			
				'contact' 		=> $contact->id,
				'contenido' 	=> DataBase::Cadena($contenido),
				'type' 			=> DataBase::Cadena($type),		
				'error_type' 	=> DataBase::Cadena($error_type),				
				'sender' 		=> DataBase::Cadena($sender),
				'exception' 	=> DataBase::Cadena($exception),
				'fecha' 		=> DataBase::Cadena($dt->format('Y-m-d H:i:s'))
				);
			if(!Database::getInstance()->insert($arr, $this->tabla_error_envio)) {
				$result = $result && false;
			}														
			
			return $result;
		 }
		 
		 public function ParseoDeArray($arr) {
			if(array_key_exists('id', $arr))
			{
				$this->id 			= $arr['id'];
			}						
			if(array_key_exists("user",$arr))
			{	
				$this->user	 		= $arr['user'];
			}				
			$this->contenido		= $arr['contenido'];			
			$this->fecha	 		= $arr['fecha'];
			if(array_key_exists("type",$arr))
			{	
				$this->type	 		= $arr['type'];
			}			
			if(array_key_exists("contact",$arr))
			{	
				$this->contact	 		= $arr['contact'];
			}
			if(array_key_exists("error_type",$arr))
			{	
				$this->error_type	 		= $arr['error_type'];
			}
			if(array_key_exists("sender",$arr))
			{	
				$this->sender	 		= $arr['sender'];
			}
			if(array_key_exists("exception",$arr))
			{	
				$this->exception	 		= $arr['exception'];
			}			
		}
		
		public function Eliminar() {						
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->delete($this->tabla_error_envio, $where)) {				
				return false;
			}						
						
			return true;			
		 }
		 
		 public function DeleteAll($logs)
		 {
		 	foreach($logs as $l)
		 	{
				$l->Eliminar();
			}		 	
		 }
		 
		 public function GetAll() {
			$sql  = "SELECT * FROM ".$this->tabla_error_envio." ORDER BY fecha DESC";			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Messages = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$newMsg = new ErrorLog();
				$newMsg->ParseoDeArray($row);
				array_push($Messages,$newMsg);
			}
						
			return $Messages;
		}	
		
		 public function GetUsername() {
			if($this->user > 0)
			{
				$u = new Usuario();
				$u->GetById($this->user);
				return $u->username;					
			}
			return "";
		}	
		
		public function GetContact() {
			if($this->contact > 0)
			{
				$u = new Contact();
				$u->GetById($this->contact,$this->user);
				return $u->number;					
			}
			return "";
		}
		
		public function GetAllToResend() {
			$sql  = "SELECT * FROM ".$this->tabla_error_envio." WHERE contact > 0 AND type != '' AND NOW() <= DATE_ADD(fecha,INTERVAL 1 WEEK) ORDER BY fecha DESC";						
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Messages = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$newMsg = new ErrorLog();
				$newMsg->ParseoDeArray($row);
				array_push($Messages,$newMsg);
			}
						
			return $Messages;
		}	
		
		public function GetById($id) {
			$sql  = "SELECT * FROM ".$this->tabla_error_envio." WHERE id = " . $id;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}
						
			$this->ParseoDeArray($arr);
		
			return true;
		}
	}	

?>