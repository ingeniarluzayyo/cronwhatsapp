<?php
class Single_Messages extends Messages{
		
		public $tabla;
		public $id;
		public $number;
		public $content;
		public $type;
		public $user;
		public $date;
		public $sender_id;
		public $clean;
		
		private $dt;
		
		public function __construct() {
			$this->tabla 			= 'single_messages';
			$this->id 				= '';			
			$this->number  			= '';
			$this->content 			= '';
			$this->type 		    = '';
			$this->user 		    = '';	
			$this->date 			= '';
			$this->sender_id 		= '';
			$this->clean 		    = '0';	
			
			$this->dt = new DateTime();			
		}
		
			
		public function ParseoDeArray($arr) {
			if(array_key_exists('id', $arr))
			{
				$this->id 				= $arr['id'];
			}
			$this->number 			= $arr['number'];									
			$this->content 			= $arr['content'];	
			$this->type 			= $arr['type'];
			$this->user 			= $arr['user'];
			$this->date 			= $arr['date'];
			$this->sender_id 		= $arr['sender_id'];
			$this->clean 			= $arr['clean'];
		}
		
		public function setTabla($tabla)
		{
			$this->tabla = $tabla;
		}
		
		public function Crear() {			
			$arr = array(
				'number' 				=> DataBase::Cadena($this->number),
				'content' 				=> DataBase::Cadena($this->content),
				'type' 					=> DataBase::Cadena($this->type),
				'user' 					=> DataBase::Cadena($this->user),
				'date' 					=> DataBase::Cadena($this->dt->format('Y-m-d H:i:s')),
				'sender_id' 			=> DataBase::Cadena($this->sender_id),
				'clean' 	 			=> DataBase::Cadena($this->clean)
				);
			if(!Database::getInstance()->insert($arr, $this->tabla)) {
				return false;
			}						
		}	

		public function GetById($id) {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE id = " . $id;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}

			$this->ParseoDeArray($arr);			
		
			return true;
		}
		
		public function Eliminar() {						
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->delete($this->tabla, $where)) {				
				return false;
			}						
						
			return true;			
		 }
		 
		public function Update_clean() {
			$arr = array(
				'clean' 	=> DataBase::Cadena($this->clean)
				);
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
				return false;
			}
			return true;
		}
		
		public function GetAll() {
			$sql  = "SELECT * FROM ".$this->tabla." ORDER BY date";			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Contenidos = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$newContenido = new Single_Messages();
				$newContenido->ParseoDeArray($row);
				array_push($Contenidos,$newContenido);
			}
						
			return $Contenidos;
		}
		
		public function Search($id_user = 0,$from = '',$to = '') {
			$sql  = "SELECT * FROM ".$this->tabla;
			$where = " WHERE 1=1";
			
			if($id_user != 0)
			{
				$where .= " AND user=".$id_user;
			}
			if($from != '')
			{
				$where .= " AND date >= '".$from."'";
			}
			if($to != '')
			{
				$where .= " AND date <= '".$to."'";
			}
			$sql .= $where . " ORDER BY date DESC";
			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Messages = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$newMsg = new Single_Messages();
				$newMsg->ParseoDeArray($row);
				array_push($Messages,$newMsg);
			}
						
			return $Messages;
		}
		
		public function GetAllToClean() {
			
			$sql  = "SELECT * FROM ".$this->tabla." WHERE TIMESTAMPDIFF(HOUR,date,'".$this->dt->format('Y-m-d H:i:s')."') >= 24 AND clean = 0  AND sender_id>0";			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Messages = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$newMsg = new Single_Messages();
				$newMsg->ParseoDeArray($row);
				array_push($Messages,$newMsg);
			}
						
			return $Messages;
		}			
}
?>