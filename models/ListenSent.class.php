<?php
	class ListenSent {
		
		public $tabla;
		public $id;
		public $sender;
		public $contenido;
		public $numero;
		public $fecha;
		public $fecha_escucha;			
		
		public function __construct() {
			$this->tabla 			= 'listens_sent';			
			$this->id 				= '';
			$this->sender			= '';
			$this->contenido		= '';
			$this->numero			= '';
			$this->fecha			= '';
			$this->fecha_escucha	= '';
		}
		
		public function ParseoDeArray($arr) {
			if(array_key_exists('id', $arr))
			{
				$this->id 			= $arr['id'];
			}
			if(array_key_exists('sender', $arr))
			{
				$this->sender			= $arr['sender'];
			}
			if(array_key_exists('contenido', $arr))
			{
				$this->contenido			= $arr['contenido'];
			}
			if(array_key_exists('numero', $arr))
			{
				$this->numero				= $arr['numero'];
			}
			if(array_key_exists('fecha', $arr))
			{
				$this->fecha				= $arr['fecha'];
			}
			if(array_key_exists('fecha_escucha', $arr))
			{
				$this->fecha_escucha		= $arr['fecha_escucha'];
			}
		}
		
		public function setTabla($tabla)
		{
			$this->tabla = $tabla;
		}

		public function Crear() {	
			$dt = new DateTime();
			$result['msg'] = "";
			$result['state'] = true;
				
			$arr = array(
				'sender'			=> $this->sender,
				'contenido' 		=> DataBase::Cadena($this->contenido),
				'numero' 			=> $this->numero,
				'fecha'				=> DataBase::Cadena($this->fecha),		
				'fecha_escucha'		=> DataBase::Cadena($dt->format('Y-m-d'))
				);
			if(!Database::getInstance()->insert($arr, $this->tabla)) {
				$result['msg'] = "Error while creating a ListenSent.";
				$result['state'] = false;
				return $result;
			}							
			
			$result['msg'] .= "The ListenSent has been created succesfully.<br />";
			
			echo $result['msg'];
			return $result;
		 }		 		
		
		public function ValidarDatos()
		 {
		 	$result['state'] = true;
		 	$result['msg'] = "";
		 	
		 	if(! ($this->numero != "" ))
		 	{
				$result['state'] = false;
		 		$result['msg'] .= "Invalid characters on name.<br />";
			}
			if(! ($this->sender != "" AND is_numeric($this->sender)))
		 	{
				$result['state'] = false;
		 		$result['msg'] .= "Error asociating the ListenSent to the user.<br />";
			}			
						
			return $result;
		 }
		 
		public function Eliminar() {
			$result['state'] = true;
			$result['msg'] 	 = "";
			
						
			if($result['state'])
			{							
				$where = "WHERE id = " . $this->id;
				if(!Database::getInstance()->delete($this->tabla, $where)) {
					$result['msg'] .= "Error while deleting a ListenSent.";
					$result['state'] = false;
					return $result;
				}
			}
			
			$result['msg'] .= "The ListenSent has been deleted succesfully.<br />";			
			return $result;			
		 }

		 public function newId() {
			$sql  = "SELECT max(id)+1 as id FROM ".$this->tabla;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return 0;
			}			
		
			return $arr["id"];
		}

		public function GetById($id) {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE id = " . $id;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}

			$this->ParseoDeArray($arr);			
		
			return true;
		}		

		public function GetAll() {
			$sql  = "SELECT * FROM ".$this->tabla." ORDER BY fecha DESC";			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Contenidos = array();
			
			//$oUsuario->ParseoDeArray($arr);
			while($row = mysqli_fetch_array($arr))
			{
				$newContenido = new ListenSent();
				$newContenido->ParseoDeArray($row);
				array_push($Contenidos,$newContenido);
			}
						
			return $Contenidos;
		}
		
		public function Search($fecha_escucha) {
			if($fecha_escucha != "")
			{						
				$sql  = "SELECT * FROM ".$this->tabla." WHERE fecha_escucha = '".$fecha_escucha."' ORDER BY fecha DESC";			
				if(!$arr = Database::getInstance()->executeQuery($sql)) {
					return false;
				}
				$Contenidos = array();
				
				//$oUsuario->ParseoDeArray($arr);
				while($row = mysqli_fetch_array($arr))
				{
					$newContenido = new ListenSent();
					$newContenido->ParseoDeArray($row);
					array_push($Contenidos,$newContenido);
				}
							
				return $Contenidos;
			}else{
				return $this->GetAll();
			}
		}								
	}	

?>