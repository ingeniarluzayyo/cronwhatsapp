<?php
require_once(__DIR__.'/Message.class.php');

class Bulk_Messages extends Messages{
		
		public $tabla;
		public $id;
		public $campaign_id;
		public $contact_id;
		public $date_sent;
		public $sender_id;
		public $sent;
		public $clean;
		
		private $array_masivo;
		private $dt;
		
		public function __construct() {
			$this->tabla 			= 'bulk_messages';
			$this->id 				= '';			
			$this->campaign_id  	= '';
			$this->contact_id 		= '';
			$this->date_sent 		= '';
			$this->sender_id 		= '0';	
			$this->sent 			= '0'; //0 not sent - 1 sent - 2 error sent
			$this->clean 		    = '0';
			
			$this->array_masivo     = array();
			$this->dt = new DateTime();				
		}
		
			
		public function ParseoDeArray($arr) {
			if(array_key_exists('id', $arr))
			{
				$this->id 				= $arr['id'];
			}
			$this->campaign_id 			= $arr['campaign_id'];									
			$this->contact_id 			= $arr['contact_id'];	
			$this->date_sent 			= $arr['date_sent'];
			$this->sender_id 			= $arr['sender_id'];
			$this->sent 				= $arr['sent'];
			$this->clean 				= $arr['clean'];
		}
		
		public function setTabla($tabla)
		{
			$this->tabla = $tabla;
		}
		
		public function Crear() {			
			$arr = array(
				'campaign_id' 				=> DataBase::Cadena($this->campaign_id),
				'contact_id' 				=> DataBase::Cadena($this->contact_id),
				'date_sent' 				=> DataBase::Cadena($this->date_sent),
				'sender_id' 				=> DataBase::Cadena($this->sender_id),
				'sent' 						=> DataBase::Cadena($this->sent),
				'clean' 	 				=> DataBase::Cadena($this->clean)
				);
			if(!Database::getInstance()->insert($arr, $this->tabla)) {
				return false;
			}		
			
			return true;				
		}
		
		public function CrearBulk() {
			$arr = array(
				'campaign_id' 				=> DataBase::Cadena($this->campaign_id),
				'contact_id' 				=> DataBase::Cadena($this->contact_id),
				'date_sent' 				=> DataBase::Cadena($this->date_sent),
				'sender_id' 				=> DataBase::Cadena($this->sender_id),
				'sent' 						=> DataBase::Cadena($this->sent),
				'clean' 	 				=> DataBase::Cadena($this->clean)
				);
			array_push($this->array_masivo,$arr);
			return true;
		}	
		
		public function Save()
		{
			if(!Database::getInstance()->insert_bulk($this->array_masivo, $this->tabla)) {
				return false;
			}
			
			return true;
		}

		public function GetById($id) {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE id = " . $id;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}

			$this->ParseoDeArray($arr);			
		
			return true;
		}
		
		public function Eliminar() {						
			$where = "WHERE campaign_id = " . $this->campaign_id;
			if(!Database::getInstance()->delete($this->tabla, $where)) {				
				return false;
			}						
						
			return true;			
		 }
		 
		public function Update_sent() {
			$arr = array(
				'sent' 	=> DataBase::Cadena($this->sent)
				);
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
				return false;
			}
			return true;
		}
		
		public function Update_clean() {
			$arr = array(
				'clean' 	=> DataBase::Cadena($this->clean)
				);
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
				return false;
			}
			return true;
		}
		
		public function Update_date() {
			$arr = array(
				'date_sent' 	=> DataBase::Cadena($this->dt->format('Y-m-d H:i:s'))
				);
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
				return false;
			}
			return true;
		}
		
		public function Update_sender() {
			$arr = array(
				'sender_id' 	=> DataBase::Cadena($this->sender_id)
				);
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
				return false;
			}
			return true;
		}
		
		public function CountSent() { 
			$sql  = "SELECT count(1) as cant FROM ".$this->tabla." WHERE campaign_id = ".$this->campaign_id." and sent=1";
			
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}						
			return $arr['cant'];
		}
		
		public function CountNotsent() { 
			$sql  = "SELECT count(1) as cant FROM ".$this->tabla." WHERE campaign_id = ".$this->campaign_id." and sent=0";
			
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}						
			return $arr['cant'];
		}
		
		public function GetAll($sent) {
			$Messages = array();						
			if($this->campaign_id > 0)
			{
				$sql  = "SELECT * FROM ".$this->tabla." WHERE campaign_id = ".$this->campaign_id." AND sent = ".$sent;			
				if(!$arr = Database::getInstance()->executeQuery($sql)) {
					return array();
				}
				
				while($row = mysqli_fetch_array($arr))
				{
					$newMsg = new Bulk_Messages();
					$newMsg->ParseoDeArray($row);
					array_push($Messages,$newMsg);
				}
			}
			return $Messages;
		}
		
		public function GetAllOfCampaign() {
			$Messages = array();						
			if($this->campaign_id > 0)
			{
				$sql  = "SELECT * FROM ".$this->tabla." WHERE campaign_id = ".$this->campaign_id." AND sent = 0";			
				if(!$arr = Database::getInstance()->executeQuery($sql)) {
					return array();
				}
				
				while($row = mysqli_fetch_array($arr))
				{
					$newMsg = new Bulk_Messages();
					$newMsg->ParseoDeArray($row);
					array_push($Messages,$newMsg);
				}
			}
			return $Messages;
		}
		
		public function GetAllToClean() {			
			$sql  = "SELECT * FROM ".$this->tabla." WHERE TIMESTAMPDIFF(HOUR,date_sent,'".$this->dt->format('Y-m-d H:i:s')."') >= 24 AND clean = 0 AND sent = 1 AND sender_id>0";			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Messages = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$newMsg = new Bulk_Messages();
				$newMsg->ParseoDeArray($row);
				array_push($Messages,$newMsg);
			}
						
			return $Messages;
		}	
}
?>