<?php
	class Usuario {
		
		public $id;
		public $username;
		public $password;
		public $email;
		public $telefono;
		public $user_type;
		public $credits;
		public $habilitado;
		public $tabla;
		
		public function __construct() {
			$this->id 				= '';
			$this->username			= '';
			$this->password 		= '';
			$this->email 			= '';
			$this->telefono 		= '';
			$this->user_type 		= '';
			$this->credits	 		= '';
			$this->habilitado 		= '';	
			$this->tabla 			= 'usuarios';
		}
		
		public function ParseoDeArray($arr) {
			if(array_key_exists('id', $arr))
			{
				$this->id 			= $arr['id'];
			}			
			$this->username			= $arr['username'];
			$this->password 		= $arr['password'];
			$this->email 			= $arr['email'];
			$this->telefono 		= $arr['telefono'];
			if(array_key_exists('credits', $arr))
			{
				$this->credits			= $arr['credits'];
			}
			if(array_key_exists('user_type', $arr))
			{
				$this->user_type		= $arr['user_type'];
			}
			$this->habilitado 		= $arr['habilitado'];
		}
		
		public function setTabla($tabla)
		{
			$this->tabla = $tabla;
		}
		public function Crear() {
			$result = $this->ValidarDatos();	
			if($result['state'])
			{							
				$arr = array(
					'username' 		=> DataBase::Cadena($this->username),
					'password' 		=> DataBase::Cadena(md5($this->password)),				
					'email' 		=> DataBase::Cadena($this->email),
					'telefono' 		=> DataBase::Cadena($this->telefono),
					'user_type'		=> DataBase::Cadena($this->user_type),
					'habilitado' 	=> DataBase::Cadena($this->habilitado)
					);
				if(!Database::getInstance()->insert($arr, $this->tabla)) {
					$result['msg'] .= "Error while creating an user.";
					$result['state'] = false;
					return $result;
				}				
			}else{
				return $result;
			}
			
			$result['msg'] .= "The user has been created succesfully.<br />";
			return $result;
		 }
		 
		public function Modificar() {
			$result = $this->ValidarDatos();	
			if($result['state'])
			{
				if($this->password != ''){
					//Para modificar los creditos usar la funcion AddCredits()
					$arr = array(
						'username' 		=> DataBase::Cadena($this->username),
						'password' 		=> DataBase::Cadena(md5($this->password)),
						'email' 		=> DataBase::Cadena($this->email),
						'telefono' 		=> DataBase::Cadena($this->telefono),
						'user_type'		=> DataBase::Cadena($this->user_type),
						'habilitado' 	=> DataBase::Cadena($this->habilitado)
						);
				}else{
					$arr = array(
						'username' 		=> DataBase::Cadena($this->username),
						'email' 		=> DataBase::Cadena($this->email),
						'telefono' 		=> DataBase::Cadena($this->telefono),
						'user_type'		=> DataBase::Cadena($this->user_type),
						'habilitado' 	=> DataBase::Cadena($this->habilitado)
						);
				}
				$where = "WHERE id = " . $this->id;
				if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
					$result['msg'] .= "Error while editing an user.";
					$result['state'] = false;
					return $result;
				}
			}else{
				return $result;
			}
			
			$result['msg'] .= "The user has been edited succesfully.<br />";
			return $result;
				
		 }
		 
		 public function ValidarDatos()
		 {
		 	$result['state'] = true;
		 	$result['msg'] = "";
		 	
		 	if(! ($this->username != "" AND is_string($this->username)))
		 	{
				$result['state'] = false;
		 		$result['msg'] .= "Invalid characters on username.<br />";
			}
			if (self::func() != 'Modificar'){
				if($this->password == "")
			 	{
					$result['state'] = false;
			 		$result['msg'] .= "You must put a password.<br />";
				}
			}
			if($this->user_type == "0")
		 	{
				$result['state'] = false;
		 		$result['msg'] .= "You must put a user type.<br />";
			}
						
			return $result;
		 }
		 
		 public function func() {
	        $debug = debug_backtrace();
	        return $debug[2]['function'];
	     } 
	    
		 public function AddCredits($credits) {	
		 	$result['state'] = true;
			$result['msg'] 	 = "";
			
		 	if(is_numeric($credits))
		 	{								
				$arr = array(
					'credits' 		=> ($this->credits + $credits)			
					);
				$where = "WHERE id = " . $this->id;
				if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
					$result['msg'] .= "Error while adding credits.";
					$result['state'] = false;
					return $result;
				}
				$result['msg'] .= "The credits has been added succesfully.<br />";			
				return $result;
			}
				$result['msg'] .= "Invalid character.";
				$result['state'] = false;
				return $result;
		 }
		 
		public function Eliminar() {
			$result['state'] = true;
			$result['msg'] 	 = "";
			
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->delete($this->tabla, $where)) {
				$result['msg'] .= "Error while deleting an users.";
				$result['state'] = false;
				return $result;
			}
			$result['msg'] .= "The user has been deleted succesfully.<br />";			
			return $result;	
		 }


		public function GetById($id) {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE id = " . $id;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}
						
			$this->ParseoDeArray($arr);
		
			return true;
		}
		
		public function IsAdmin($id) {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE user_type = 'admin' AND id = " . $id;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}
		
			return true;
		}
		
		public function GetByUsername($username) {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE username = '".$username."'";
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}
						
			$this->ParseoDeArray($arr);
		
			return true;
		}
		
		public function GetUsername($id_user) { 
			$sql  = "SELECT username FROM ".$this->tabla." WHERE id = " . $id_user;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}						
			return $arr['username'];
		}
		
		public function GetAll() {
			$sql  = "SELECT * FROM ".$this->tabla;			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Usuarios = array();
			
			//$oUsuario->ParseoDeArray($arr);
			while($row = mysqli_fetch_array($arr))
			{
				$newUser = new Usuario();
				$newUser->ParseoDeArray($row);
				array_push($Usuarios,$newUser);
			}
						
			return $Usuarios;
		}
		
	}	

?>