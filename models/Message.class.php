<?php
class Messages{
		
		public $tabla;
		public $id;
		public $campaign_id;
		public $contact_id;
		public $date_sent;
		public $sender_id;
		public $sent;
		public $clean;
		
		private $array_masivo;
		private $dt;
		
		public function __construct() {
			$this->tabla 			= 'bulk_messages';
			$this->id 				= '';			
			$this->campaign_id  	= '';
			$this->contact_id 		= '';
			$this->date_sent 		= '';
			$this->sender_id 		= '0';	
			$this->sent 			= '0'; //0 not sent - 1 sent - 2 error sent
			$this->clean 		    = '0';
			
			$this->array_masivo     = array();
			$this->dt = new DateTime();				
		}
		
			
		public function ParseoDeArray($arr) {
			if(array_key_exists('id', $arr))
			{
				$this->id 				= $arr['id'];
			}
			$this->campaign_id 			= $arr['campaign_id'];									
			$this->contact_id 			= $arr['contact_id'];	
			$this->date_sent 			= $arr['date_sent'];
			$this->sender_id 			= $arr['sender_id'];
			$this->sent 				= $arr['sent'];
			$this->clean 				= $arr['clean'];
		}
		
		public function setTabla($tabla)
		{
			$this->tabla = $tabla;
		}
		
		public function Send($user,$contact,$tipo,$contenido,$utf8 = FALSE) {
			$sender = new Sender();
			$sender = $sender->GetSenderAvailable($user);
	
			
			if($sender->id == 0){return 0;}													

			try{
				if($tipo == 'text')
				{
					$this->SendText($user,$sender,$contenido,$contact,$utf8);
				}else{
					$this->SendMultimedia($user,$sender,$contenido,$contact,$tipo,$utf8);
				}
			}catch(Exception $e){
				//echo "Login Faild Error <br />";
				//$sender->habilitado = 0;
				//$sender->Update_available();
				$sender->assigned_user = 0;
				$sender->AssignToUser();
				var_dump($e);
				return 0;
			}
			
			$sender->assigned_user = 0;
			$sender->AssignToUser();
			
			return $sender->id; 
		}
		
		private function SendText($user,$sender,$content,$contact,$utf8)
		{
			$wa = new WhatsProt($sender->numero, "WhatsApp", false);
							
			$replace= array('�','�','�','�', '�', '�', '�', '�', '�','�', '�', '�', '�', '�', '�');
			$find = array('&#8226;','&ordm;','&ordm;','&aacute;', '&eacute;', '&iacute;', '&oacute;', '&uacute;', '&ntilde;','&Aacute;', '&Eacute;', '&Iacute;', '&Oacute;', '&Uacute;', '&Ntilde;');
			$content = str_ireplace($find,$replace,$content);
			if(strstr($content,'{name}') != false){$contenido = str_replace('{name}',$contact->name,utf8_encode($content));}
		 	
		 	$wa->connect();
			$wa->loginWithPassword(trim($sender->password));
			$wa->sendSetProfilePicture($sender->ProfileImg($user));
			
			
			
			/**
			* delta Sync
			* incremental contacts sync. It will add one or more contacts 
			* to the existing contacts list. It should be used when you are 
			* interacting with a new contact
			**/
			$newContact = array($contact->number);
			$wa->sendSync($newContact);
			
			if($utf8){
				if($wa->sendMessage($contact->number, $content)!= "")
				{
					$wa->pollMessage();	
					$sender->Update_enviados(1);
					return true;
				}	
			}else{
				if($wa->sendMessage($contact->number, utf8_encode($content))!= "")
				{
					$wa->pollMessage();	
					$sender->Update_enviados(1);
					return true;
				}
			}
			
			
			return false;
		}
		
		private function SendMultimedia($user,$sender,$content,$contact,$type,$utf8)
		{
			$wa = new WhatsProt($sender->numero, "WhatsApp", false);
			$hashMedia = new HashMedia();
				
			//Porque viene de la base de datos con ;
			$replace= array('�','�','�','�', '�', '�', '�', '�', '�','�', '�', '�', '�', '�', '�');
			$find = array('&#8226;','&ordm;','&ordm;','&aacute;', '&eacute;', '&iacute;', '&oacute;', '&uacute;', '&ntilde;','&Aacute;', '&Eacute;', '&Iacute;', '&Oacute;', '&Uacute;', '&Ntilde;');
			
			$content = str_ireplace($find,$replace,$content);
					
			$array_contenido = explode(';',$content);
			
			if(count($array_contenido) == 2)
			{
				$contenido 	= $array_contenido[0];
				$caption 	= $array_contenido[1];	
			}else{
				$contenido 	= $array_contenido[0];
				$caption = '';
			}		
			
			//por si tiene acentos y eso con ; 
			if(count($array_contenido)>2){
				for($i=2;$i<count($array_contenido);$i++){
					$caption .= ';'.$array_contenido[$i];
				}
			}
						
			$filehash = base64_encode(hash_file("sha256", $contenido, true));
			$filesize = filesize($contenido);
			
			//reemplazo caracteres
			/*$replace= array('�','�','�','�', '�', '�', '�', '�', '�','�', '�', '�', '�', '�', '�');
			$find = array('&#8226;','&ordm;','&ordm;','&aacute;', '&eacute;', '&iacute;', '&oacute;', '&uacute;', '&ntilde;','&Aacute;', '&Eacute;', '&Iacute;', '&Oacute;', '&Uacute;', '&Ntilde;');
			
			$caption = str_ireplace($find,$replace,$caption);*/
			
			$estaElHash = $hashMedia->GetByHash($filehash);															 
			
			if($estaElHash)
			{				
				$dtHash = new DateTime($hashMedia->fecha);
				$dtNow = new DateTime("now");
				$diferencia = $dtNow->diff($dtHash);	
			}	
								
			if($estaElHash && !$diferencia->d < 1)
			{						
				$hashMedia->Modificar();	
			}
			$wa->connect();
			$wa->loginWithPassword(trim($sender->password));
			$wa->sendSetProfilePicture($sender->ProfileImg($user));
			
			/**
			* delta Sync
			* incremental contacts sync. It will add one or more contacts 
			* to the existing contacts list. It should be used when you are 
			* interacting with a new contact
			**/
			$newContact = array($contact->number);
			$wa->sendSync($newContact);
			
			$result = "";
			switch($type)
			{
				case "image":{
					if($utf8){
						$result = $wa->sendMessageImage($contact->number, $contenido,false,$filesize,($estaElHash)?$filehash:'',$caption);
					}else{
						$result = $wa->sendMessageImage($contact->number, $contenido,false,$filesize,($estaElHash)?$filehash:'',utf8_encode($caption));
					}
					break;
				}
				case "audio":{
					$result = $wa->sendMessageAudio($contact->number, $contenido,false,$filesize,($estaElHash)?$filehash:'');
					break;
				}
				case "video":{
					if($utf8){
						$result = $wa->sendMessageVideo($contact->number, $contenido,false,$filesize,($estaElHash)?$filehash:'',$caption);
					}else{
						$result = $wa->sendMessageVideo($contact->number, $contenido,false,$filesize,($estaElHash)?$filehash:'',utf8_encode($caption));
					}
					break;
				}
			}
			
			if($result != "")
			{
				$wa->pollMessage();	
				$sender->Update_enviados(1);
				if($estaElHash)
				{
					$hashMedia->hash = $filehash;
					$hashMedia->Crear();
				}								
				return true;		
			}											
			return false;	 					 
		}
}
?>