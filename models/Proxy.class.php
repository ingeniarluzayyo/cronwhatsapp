<?php
	class Proxy {
		
		public $id;
		public $ip;
		public $puerto;
		public $tipo;
		public $user;
		public $password;
		public $autentication;
		public $estado;
		public $habilitado;
		public $tabla;
		
		public $arr_auth = array(
					CURLAUTH_ANY=>'ANY',
					CURLAUTH_ANYSAFE=>'ANYSAFE',
					CURLAUTH_BASIC=>'BASIC',
					CURLAUTH_DIGEST=>'DIGEST',
					CURLAUTH_GSSNEGOTIATE=>'GSSNEGOTIATE',
					CURLAUTH_NTLM=>'NTLM'
				);
		
		public function __construct() {
			$this->id 				= '';
			$this->ip				= '';
			$this->puerto			= '';
			$this->tipo				= '';
			$this->user 			= '';
			$this->password 		= '';
			$this->autentication 	= '';
			$this->habilitado 		= '';
			$this->estado   		= '';	
			$this->tabla 			= 'proxy';
		}
		
		public function ParseoDeArray($arr) {
			$this->id 				= $arr['id'];
			$this->ip				= $arr['ip'];
			$this->puerto			= $arr['puerto'];
			$this->tipo				= $arr['tipo'];
			
			if(array_key_exists('user', $arr))
			{
				$this->user 			= $arr['user'];
			}
			if(array_key_exists('password', $arr))
			{
				$this->password 			= $arr['password'];
			}
			if(array_key_exists('autentication', $arr))
			{
				$this->autentication 			= $arr['autentication'];
			}
			if(array_key_exists('estado', $arr))
			{
				$this->estado 			= $arr['estado'];
			}
			if(array_key_exists('habilitado', $arr))
			{
				$this->habilitado 			= $arr['habilitado'];
			}
		}
		
		public function setTabla($tabla)
		{
			$this->tabla = $tabla;
		}
		public function GetArrTipos()
		{
			return array('HTTP'=>'HTTP',CURLPROXY_SOCKS4=>'Sock 4',CURLPROXY_SOCKS5=>'Sock 5');
		}
		public function GetArrAuth()
		{
			return $this->arr_auth;
		}
		
		public function GetArr()
		{
			$sql  = "SELECT * FROM ".$this->tabla." WHERE estado='Online' AND habilitado='1'";			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Contenidos = array();						
			
			while($row = mysqli_fetch_array($arr))
			{				
				$Contenidos[$row["id"]] = $row["ip"].":".$row["puerto"];
			}
						
			return $Contenidos;
		}
		
		public function GetTipo()
		{
			switch($this->tipo)
			{
				case "HTTP":return "HTTP";
				case CURLPROXY_SOCKS4:return "Sock 4";
				case CURLPROXY_SOCKS5:return "Sock 5";
				default: return "Otro";
			}
		}
		
		public function GetCantProxys()
		{
			$sql  = "SELECT count(1) as cant FROM ".$this->tabla;

			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return "0";
			}
						
			return $arr['cant'];
		}
		
		public function GetAutentication()
		{
			if($this->autentication == "")
			{
				return "";
			}
			return $this->arr_auth[$this->autentication];
		}
		
		public function Crear() {		
			if($this->autentication == 0){$this->autentication = "";}
			$arr = array(
				'ip' 			=> DataBase::Cadena($this->ip),
				'puerto' 		=> $this->puerto,
				'tipo' 			=> DataBase::Cadena($this->tipo),	
				'user' 			=> DataBase::Cadena($this->user),			
				'password' 		=> DataBase::Cadena($this->password),
				'autentication' => DataBase::Cadena($this->autentication),
				'estado' 		=> DataBase::Cadena($this->estado),
				'habilitado' 	=> DataBase::Cadena($this->habilitado)
				);
			if(!Database::getInstance()->insert($arr, $this->tabla)) {
				$result['state'] = false;
				$result['msg'] = "An error occurred when creating the proxy.";
				return $result;	
			}
			$result['state'] = true;
			$result['msg'] = "The proxy has been created successfully.";
			return $result;
		 }
		 
		public function Modificar() {
			if($this->autentication == 0){$this->autentication = "";}
			$arr = array(
				'ip' 			=> DataBase::Cadena($this->ip),
				'puerto' 		=> $this->puerto,
				'tipo' 			=> DataBase::Cadena($this->tipo),	
				'user' 			=> DataBase::Cadena($this->user),			
				'password' 		=> DataBase::Cadena($this->password),
				'autentication' => DataBase::Cadena($this->autentication),
				'habilitado' 	=> DataBase::Cadena($this->habilitado)
				);
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
				$result['state'] = false;
				$result['msg'] = "An error occurred when editing the proxy.";
				return $result;	
			}
			$result['state'] = true;
			$result['msg'] = "The proxy has been edited successfully.";
			return $result;
		 }
		 
		public function Eliminar() {
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->delete($this->tabla, $where)) {
				$result['state'] = false;
				$result['msg'] = "An error occurred when deleting the proxy.";
				return $result;
			}
			$result['state'] = true;
			$result['msg'] = "The proxy has been delete successfully.";
			return $result;
		 }
		 
		public function UpdateStatus() {
			$arr = array(
				'estado' 			=> DataBase::Cadena($this->estado)
				);
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
				$result['state'] = false;
				$result['msg'] = "An error occurred when updating the proxy status.";
				return $result;	
			}
			
			if($this->estado == 'Online'){
				$result['state'] = true;
				$result['msg'] = "The proxy is Online.";
			}else{
				$result['state'] = false;
				$result['msg'] = "The proxy is Offline.";
			}
			
			return $result;
		 }
		 
		public function isOnline($url){
				$theHeader = curl_init($url);
				curl_setopt($theHeader, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($theHeader, CURLOPT_TIMEOUT, 15);
				curl_setopt($theHeader, CURLOPT_PROXY, $this->ip.":".$this->puerto); 
				 
				//Execute the request
				$curlResponse = curl_exec($theHeader);
				 
				if ($curlResponse === false) 
				{
					return 'Offline';
				} 
				else 
				{
 					return 'Online';
				}
		}
		
		public function Validar(){
			
			$result['state'] = true;
			
			if(filter_var($this->ip, FILTER_VALIDATE_IP) === false){
				$result['state'] = false;
				$result['msg'] = "The field IP have wrong format.";
				return $result;
			}
			
			if(!is_numeric($this->puerto)){
				$result['state'] = false;
				$result['msg'] = "The field port only accept numeric character.";
				return $result;
			}
			
			if(empty($this->tipo)){
				$result['state'] = false;
				$result['msg'] = "You have to choose a type proxy.";
				return $result;
			}

			return $result;
		}


		public function GetById($id) {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE id = " . $id;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}
						
			$this->ParseoDeArray($arr);
		
			return true;
		}
		
		public function GetAll() {
			$sql  = "SELECT * FROM ".$this->tabla;			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$list = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$new = new Proxy();
				$new->ParseoDeArray($row);
				array_push($list,$new);
			}
						
			return $list;
		}
		
		public function GetAvailable() {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE habilitado ='1'";			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$list = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$new = new Proxy();
				$new->ParseoDeArray($row);
				array_push($list,$new);
			}
						
			return $list;
		}
		
	}	

?>