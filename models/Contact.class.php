<?php
	class Contact {
		
		public $tabla;
		public $id;
		public $id_group;
		public $name;
		public $number;
		public $whatsapp_install;
		public $state;
		
		public $array_masivo=array();
		
		public function __construct() {
			$this->tabla 			= 'contacts';
			$this->tabla_groups 	= 'groups';
			$this->id 				= '';
			$this->id_group			= '';
			$this->name				= '';
			$this->number			= '';
			$this->whatsapp_install	= '';
			$this->state			= '';
		}
		
		public function ParseoDeArray($arr) {
			if(array_key_exists('id', $arr))
			{
				$this->id 			= $arr['id'];
			}
			$this->id_group			= $arr['id_group'];
			$this->name				= $arr['name'];
			$this->number			= $arr['number'];
			if(array_key_exists('whatsapp_install', $arr))
			{
			   $this->whatsapp_install	= $arr['whatsapp_install'];
			}
			$this->state			= $arr['state'];

		}
		
		public function setTabla($tabla)
		{
			$this->tabla = $tabla;
		}

		public function Crear() {			
			$result = $this->ValidarDatos();			
			if($result['state'])
			{						
				$arr = array(
					'id_group' 	=> DataBase::Cadena($this->id_group),
					'name' 		=> DataBase::Cadena(str_replace("'","",$this->name)),
					'number' 	=> DataBase::Cadena($this->number),
					'whatsapp_install' 	=> DataBase::Cadena('Not yet verified'),
					'state'		=> DataBase::Cadena($this->state)
					);
				if(!Database::getInstance()->insert($arr, $this->tabla)) {
					$result['msg'] .= "Error while creating a contact.";
					$result['state'] = false;
					return $result;
				}				
			}else{
				return $result;
			}
			$result['msg'] .= "The contact has been created succesfully.<br />";
			
			return $result;
		 }
		 
		public function Create_bulk_insert($group,$name,$number,$state) {
			$arr = array(
					'id_group' 			=> DataBase::Cadena($group),
					'name' 				=> DataBase::Cadena(str_replace("'","",$name)),
					'number' 			=> DataBase::Cadena($number),
					'whatsapp_install' 	=> DataBase::Cadena('Not yet verified'),
					'state'				=> DataBase::Cadena($state)
					);

			array_push($this->array_masivo,$arr);

			return true;
		}
		
		public function Save_contact_list()
		{
			if(!Database::getInstance()->insert_bulk($this->array_masivo, $this->tabla)) {
				return false;
			}
			
			return true;
		}
		 
		public function Modificar() {
			$result = $this->ValidarDatos();	
			if($result['state'])
			{
				$arr = array(
					'id_group' 	=> DataBase::Cadena($this->id_group),
					'name' 		=> DataBase::Cadena($this->name),
					'number' 	=> DataBase::Cadena($this->number),
					'whatsapp_install' 	=> DataBase::Cadena('Not yet verified'),
					'state'		=> DataBase::Cadena($this->state)
					);
				$where = "WHERE id = " . $this->id;
				if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
					$result['msg'] .= "Error while editing a contact.";
					$result['state'] = false;
					return $result;
				}				
			}else{
				return $result;
			}
			
			$result['msg'] .= "The contact has been edited succesfully.<br />";
			return $result;
		 }
		 
		public function Modificar_Whatsapp_Install($numero,$estado) {
				$arr = array(
					'whatsapp_install' 	=> DataBase::Cadena($estado)
					);
				$where = "WHERE number = '".$numero."'";
				if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
					
					return false;
				}				
			return true;
		 }
		
		public function ValidarDatos()
		 {
		 	$result['state'] = true;
		 	$result['msg'] = "";
		 	
		 	if(! ($this->id_group != 0 AND is_numeric($this->id_group)))
		 	{
				$result['state'] = false;
		 		$result['msg'] .= "The contact must be assigned to a group.<br />";
			}
			if(! ($this->name != "" AND is_string($this->name)))
		 	{
				$result['state'] = false;
		 		$result['msg'] .= "The contact must have a name<br />";
			}
			if(! ($this->number != ""))
		 	{
				$result['state'] = false;
		 		$result['msg'] .= "The contact must have a number<br />";
			}		
			return $result;
		 }
		 
		public function Eliminar() {
			$result['state'] = true;
			$result['msg'] 	 = "";
			
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->delete($this->tabla, $where)) {
				$result['msg'] .= "Error while deleting a contact.";
				$result['state'] = false;
				return $result;
			}
			
			$result['msg'] .= "The contact has been deleted succesfully.<br />";			
			return $result;
		 }
		 
		 public function EliminarInsertado() {
			$result['state'] = true;
			$result['msg'] 	 = "";
			
			$where = "WHERE name = '" . $this->name."' AND number = '".$this->number."' AND id_group= ".$this->id_group;
			if(!Database::getInstance()->delete($this->tabla, $where)) {
				$result['msg'] .= "Error while deleting a contact.";
				$result['state'] = false;
				return $result;
			}
			
			$result['msg'] .= "The contact has been deleted succesfully.<br />";			
			return $result;
		 }
		 
		 public function EliminarContactosInsertado($contactos) {
			foreach($contactos as $c)
			{
				$c->EliminarInsertado();
			}
		 }
		 
		 public function Eliminar_WA_Install_No($id_user) {
			$result['state'] = true;
			$result['msg'] 	 = "";
			
			$where = "WHERE whatsapp_install = 'No' AND ".$id_user."=(SELECT id_user FROM groups WHERE id = id_group LIMIT 0,1)";
			if(!Database::getInstance()->delete($this->tabla, $where)) {
				$result['msg'] .= "Error while deleting a contacts.";
				$result['state'] = false;
				return $result;
			}
			
			$result['msg'] .= "The contacts without WhatsApp has been deleted succesfully.<br />";			
			return $result;
		 }

		 public function newId() {
			$sql  = "SELECT max(id)+1 as id FROM ".$this->tabla;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return 0;
			}			
		
			return $arr["id"];
		}

		public function GetById($id,$id_user=0) {
			
			if($id_user == 0){
				$sql  = "SELECT * FROM ".$this->tabla." WHERE id = " . $id;
			}else{
				$sql  = "SELECT * FROM ".$this->tabla." WHERE id = " . $id." AND ".$id_user."=(SELECT id_user FROM groups WHERE id = id_group LIMIT 0,1)";
			}

			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}

			$this->ParseoDeArray($arr);			
		
			return true;
		}		
		
		public function GetAllTotal($id_user,$start = 0) {
			$sql  = "SELECT count(1) as cantidad FROM ".$this->tabla." WHERE ".$id_user."=(SELECT id_user FROM groups WHERE id = id_group LIMIT 0,1) ORDER BY name LIMIT 0,1";			
						
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}
			
						
			return $arr['cantidad'];
		}
		
		public function GetAll($id_user,$start = -1) {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE ".$id_user."=(SELECT id_user FROM groups WHERE id = id_group LIMIT 0,1) ORDER BY name";			
			
			if($start!=-1)
			{
				$sql .= " LIMIT ".$start.",".CANTIDAD_ITEMS_PAGINA;	
			}
			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Contenidos = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$newContenido = new Contact();
				$newContenido->ParseoDeArray($row);
				array_push($Contenidos,$newContenido);
			}
						
			return $Contenidos;
		}
		
		public function IsAnyInProgress() {
			$sql  = "SELECT count(1) as cant FROM ".$this->tabla." WHERE whatsapp_install = 'In progress'";
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}			
		
			return ($arr['cant']>0);
		}	
		
		public function PutInProgress() {			
			$arr = array(
				'whatsapp_install' 	=> DataBase::Cadena('In progress')
				);
			$where = "whatsapp_install = 'Not yet verified' LIMIT 1000";
			if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
				return false;
			}				
			
			return true;
		 }
		 
		 public function PutInNotYetVerified() {			
			$arr = array(
				'whatsapp_install' 	=> DataBase::Cadena('Not yet verified')
				);
			$where = "whatsapp_install = 'In progress'";
			if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
				return false;
			}				
			
			return true;
		 }
		
		public function GetAllInProgress() {
			$sql  = "SELECT number FROM ".$this->tabla." WHERE whatsapp_install = 'Not yet verified' LIMIT 0,1000";			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Contenidos = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				array_push($Contenidos,$row['number']);
			}
						
			return $Contenidos;
		}
		
		public function GetAllOfGroupTotal($group,$id_user) {
			$sql  = "SELECT count(1) as cantidad FROM ".$this->tabla." WHERE id_group = ".$group." AND ".$id_user."=(SELECT id_user FROM groups WHERE id = id_group LIMIT 0,1) ORDER BY name";						
						
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}
			
						
			return $arr['cantidad'];
		}
		
		public function GetAllOfGroup($group,$id_user,$start=-1) {
			if($group == 0)
			{
				return $this->GetAll($id_user,$start);
			}
			
			$sql  = "SELECT * FROM ".$this->tabla." WHERE id_group = ".$group." AND ".$id_user."=(SELECT id_user FROM groups WHERE id = id_group LIMIT 0,1) ORDER BY name";			
			if($start!=-1)
			{
				$sql .= " LIMIT ".$start.",".CANTIDAD_ITEMS_PAGINA;	
			}
			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}						
			
			$Contenidos = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$newContenido = new Contact();
				$newContenido->ParseoDeArray($row);
				array_push($Contenidos,$newContenido);
			}
						
			return $Contenidos;
		}	
		
		public function GetContactOfGroup($group,$id_user,$cant = 0) {
			if($cant == 0){
				$sql  = "SELECT * FROM ".$this->tabla." WHERE whatsapp_install='Yes' AND ".$id_user."=(SELECT id_user FROM groups WHERE id = id_group LIMIT 0,1) AND id_group =".$group." ORDER BY id";								
			}else{
				$sql  = "SELECT * FROM ".$this->tabla." WHERE whatsapp_install='Yes' AND ".$id_user."=(SELECT id_user FROM groups WHERE id = id_group LIMIT 0,1) AND id_group =".$group." ORDER BY id LIMIT 0,".$cant;								
			}
			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			
			$Contenidos = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$newContenido = new Contact();
				$newContenido->ParseoDeArray($row);
				array_push($Contenidos,$newContenido);
			}
						
			return $Contenidos;
		}
		
		public function SearchContact($whatsapp_install = 0,$name = "",$number = "",$id_group = 0,$id_user,$start = -1) {			
			$where = $id_user."=(SELECT id_user FROM groups WHERE id = id_group LIMIT 0,1)";

			if($id_group != 0){
				$where .= " AND id_group = ".$id_group;	
			}
			if($name != ""){
				$where .= " AND name LIKE '%".$name."%'";
			}
			if($number != ""){
				$where .= " AND number LIKE '%".$number."%'";
			}
			
			if(!is_numeric($whatsapp_install) && $whatsapp_install != ''){
				$where .= " AND whatsapp_install = '".$whatsapp_install."'";
			}
						
			$sql  = "SELECT * FROM ".$this->tabla." WHERE ".$where." ORDER BY number,name";	
						
			if($start!=-1)
			{
				$sql .= " LIMIT ".$start.",".CANTIDAD_ITEMS_PAGINA;	
			}
									
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Contenidos = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$newContenido = new Contact();
				$newContenido->ParseoDeArray($row);
				array_push($Contenidos,$newContenido);
			}
						
			return $Contenidos;
		}
		
		public function SearchContactTotal($whatsapp_install = 0,$name = "",$number = "",$id_group = 0,$id_user) {			
			$where = $id_user."=(SELECT id_user FROM groups WHERE id = id_group LIMIT 0,1)";

			if($id_group != 0){
				$where .= " AND id_group = ".$id_group;	
			}
			if($name != ""){
				$where .= " AND name LIKE '%".$name."%'";
			}
			if($number != ""){
				$where .= " AND number LIKE '%".$number."%'";
			}
			
			if(!is_numeric($whatsapp_install) && $whatsapp_install != ''){
				$where .= " AND whatsapp_install = '".$whatsapp_install."'";
			}
						
			$sql  = "SELECT count(1) as cantidad FROM ".$this->tabla." WHERE ".$where." ORDER BY number,name";	
						
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}			
						
			return $arr['cantidad'];
		}
		
		public function GetGroupName() {
			$sql  = "SELECT name FROM ".$this->tabla_groups." WHERE id = " . $this->id_group;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return "";
			}

			return $arr['name'];			
		}				
		
		public function ParseoCSV($filepath,$group,$prefijo,$cant_caracteres) {
			$msg = "";
			$contenido = ManejoArchivos::contenido_archivo($filepath);
			
			$contact_to_insert = new Contact();
			
			$arr_contenido = explode("\n",$contenido);
			
			$i=1;
			foreach($arr_contenido as $linea)
			{
				$bien_definida = strstr($linea,",");
				if($bien_definida != false)
				{									
					$contacto = explode(',',$linea);
					if(count($contacto) == 2)
					{
						//Limpiar el contacto
						$conservar = '0-9'; // juego de caracteres a conservar
						$regex = sprintf('~[^%s]++~i', $conservar); // case insensitive
						$numero = preg_replace($regex, '', $contacto[1]);
						$numero = trim($numero);
						
						if(!is_numeric($numero))
						{
							$msg .= "Error in line $i: ".$linea."<br/>";
							$i++;
							continue;
						}
						
						$conservar = '0-9a-zñáéíóú '; // juego de caracteres a conservar
						$regex = sprintf('~[^%s]++~i', $conservar); // case insensitive
						$contacto[0] = preg_replace($regex, '', $contacto[0]);
					
						$name 	= (empty($contacto[0]))?' ':$contacto[0];
						$number 	= $prefijo.substr($numero,(-1)*$cant_caracteres);
						$state 	= 1;
						
						$contact_to_insert->Create_bulk_insert($group,$name,$number,$state);												
					}else{
						$linea = trim($linea);
						if(!empty($linea))
						{
							$msg .= "Error in line $i: ".$linea."<br/>";	
						}
					}	
				}else{
					$linea = trim($linea);
					if(!empty($linea))
					{
						$msg .= "Error format in line $i: ".$linea."<br/>";
					}
				}		
				$i++;		
			}
			
			$result['contacts_to_insert'] = $contact_to_insert; //Contactos a insertar
			$result['contacts_error'] = $msg; //Contactos error
						
			return $result;			
		}
				
		public function ParseoCSVGmail($filepath,$group,$prefijo,$cant_caracteres)
		{
			$msg = "";
			$contenido = ManejoArchivos::contenido_archivo($filepath);
			
			$contact_to_insert = new Contact();
			
			$contenido = str_replace("\t", "",  iconv('UTF-16', 'UTF-8',$contenido));

			$arr_contenido = explode("\n",$contenido);
			$posicion_celular = -1;
			$i=1;
			foreach($arr_contenido as $linea)
			{
				if($i>1)
				{												
					$bien_definida = strstr($linea,",");
					if($bien_definida != false)
					{
						$contacto = explode(',',$linea);	
						
						$posicion_celular = $this->buscarPalabra("Mobile",$contacto)+1;												
						
						if( count($contacto) >= $posicion_celular && $posicion_celular >= 0)
						{															
							$conservar = '0-9'; // juego de caracteres a conservar
							$regex = sprintf('~[^%s]++~i', $conservar); // case insensitive
							$numero = preg_replace($regex, '', $contacto[$posicion_celular]);
							$numero = trim($numero);
							
							if(strlen($numero) > 1 && ctype_digit($numero))
							{	
								$conservar = '0-9a-zñáéíóú '; // juego de caracteres a conservar
								$regex = sprintf('~[^%s]++~i', $conservar); // case insensitive
								$contacto[0] = preg_replace($regex, '', $contacto[0]);
							
								$name 	= (strlen($contacto[0])>0)?$contacto[0]:" ";							
								$number = $prefijo.substr($numero,(-1)*$cant_caracteres);																
								$state 	= 1;
								//echo "<br />$i";print_r($contacto);
								
								$contact_to_insert->Create_bulk_insert($group,$name,$number,$state);
																		
							}else{
								if($posicion_celular != -1 && strlen($numero)<19)
								{																										
									$linea = trim($linea);
									if(!empty($linea))
									{
										$msg .= "Error in line $i: ".$linea."<br/>";	
									}								
								}
							}
						}			
					}else{
						$linea = trim($linea);
						if(!empty($linea))
						{
							$msg .= "Error format in line $i: ".$linea."<br/>";
						}
					}
				}	
				$i++;	
			}
			
			$result['contacts_to_insert'] = $contact_to_insert; //Contactos a insertar
			$result['contacts_error'] = $msg; //Contactos error
						
			return $result;	
		}
		
		public function buscarPalabra($palabra,$arr)
		{
			foreach($arr as $key=>$value)
			{
				if($value == $palabra)
				{
					return $key;
				}
			}
			return -2;
		}
					
		public function ImportCSV($filepath,$group,$prefijo="",$cant_caracteres=0) {			
			$result = $this->ParseoCSV($filepath,$group,$prefijo,$cant_caracteres);
			
			$resultado = array();

			if(!empty($result['contacts_to_insert']))
			{
				if($result['contacts_to_insert']->Save_contact_list())
				{
					$imported_successfully = count($result['contacts_to_insert']->array_masivo);
				}else
				{
					$imported_successfully = '0 (Error while inserting contacts to database)';
				}
			}		
			
			$resultado['imported_successfully'] = $imported_successfully;
			$resultado['import_failed'] = $result['contacts_error'];
			
			return $resultado;
		}	
						
		public function ImportCSVGmail($filepath,$group,$prefijo="",$cant=0) {			
			$result = $this->ParseoCSVGmail($filepath,$group,$prefijo,$cant);
			
			$resultado = array();

			if(!empty($result['contacts_to_insert']))
			{
				if($result['contacts_to_insert']->Save_contact_list())
				{
					$imported_successfully = count($result['contacts_to_insert']->array_masivo);
				}else
				{
					$imported_successfully = '0 (Error while inserting contacts to database)';
				}
			}		
			
			$resultado['imported_successfully'] = $imported_successfully;
			$resultado['import_failed'] = $result['contacts_error'];
			
			return $resultado;
		}
	}	

?>