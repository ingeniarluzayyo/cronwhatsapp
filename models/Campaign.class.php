<?php
class Campaign{
		
		public $tabla;
		public $id;
		public $content;
		public $date;
		public $type;
		public $group_id;
		public $status;
		public $cant_total;
		
		private $dt;
		
		public function __construct() {
			$this->tabla 			= 'campaign';
			$this->id 				= '';			
			$this->content  		= '';
			$this->date 			= '';
			$this->type 			= '';
			$this->group_id 		= '';	
			$this->status 			= 'waiting';
			$this->cant_total 		= '';				
			
			$this->dt = new DateTime();
		}
		
			
		public function ParseoDeArray($arr) {
			if(array_key_exists('id', $arr))
			{
				$this->id 			= $arr['id'];
			}
			$this->content 			= $arr['content'];									
			$this->date 			= $arr['date'];	
			$this->type 			= $arr['type'];
			$this->group_id 		= $arr['group_id'];
			$this->status 			= $arr['status'];
			$this->cant_total 		= $arr['cant_total'];
		}
		
		public function setTabla($tabla)
		{
			$this->tabla = $tabla;
		}
		
		public function Crear() {			
			$arr = array(
				'content' 				=> DataBase::Cadena($this->content),
				'date' 				    => DataBase::Cadena($this->dt->format('Y-m-d H:i:s')),
				'type' 				    => DataBase::Cadena($this->type),
				'group_id' 				=> DataBase::Cadena($this->group_id),
				'status' 				=> DataBase::Cadena($this->status),
				'cant_total' 	 		=> DataBase::Cadena($this->cant_total)
				);
			if(!Database::getInstance()->insert($arr, $this->tabla)) {
				return false;
			}
			
			return true;						
		}	

		public function GetById($id) {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE id = " . $id;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}

			$this->ParseoDeArray($arr);			
		
			return true;
		}
		
		public function Eliminar() {						
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->delete($this->tabla, $where)) {				
				return false;
			}						
						
			return true;			
		 }
		 
		public function Update_Status() {
			$arr = array(
				'status' 	=> DataBase::Cadena($this->status)
				);
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
				return false;
			}
			return true;
		}
		
		public function GetAllbyStatus() {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE status='".$this->status."' ORDER BY date";			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Contenidos = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$newContenido = new Campaign();
				$newContenido->ParseoDeArray($row);
				array_push($Contenidos,$newContenido);
			}
						
			return $Contenidos;
		}
		
		public function GetQueues($user = 0) {
			if($user != 0){
				$sql  = "SELECT * FROM ".$this->tabla." WHERE status != 'finish' and group_id IN (SELECT id FROM groups where id_user=".$user.") ORDER BY date";
			}else{
				$sql  = "SELECT * FROM ".$this->tabla." WHERE status != 'finish' ORDER BY date";
			}
			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Contenidos = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$newContenido = new Campaign();
				$newContenido->ParseoDeArray($row);
				array_push($Contenidos,$newContenido);
			}
						
			return $Contenidos;
		}
		
		public function GetFirstQueue() {
			$sql  = "SELECT c.id,c.content,c.date,c.type,c.group_id,c.status,c.cant_total FROM ".$this->tabla." as c INNER JOIN groups as g ON g.id = c.group_id WHERE status = 'waiting' AND NOT EXISTS(SELECT * FROM campaign as c_aux INNER JOIN groups as g_aux ON c_aux.group_id = g_aux.id WHERE status = 'in progress' AND g_aux.id_user = g.id_user) ORDER BY date LIMIT 0,1";
			
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}

			$this->ParseoDeArray($arr);			
		
			return true;
		}
		
		public function LastId() {
			$sql  = "SELECT max(id) as id FROM ".$this->tabla;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return 0;
			}			
		
			return $arr["id"];
		}
		
		public function Search($id_user,$from,$to) {
			$sql  = "SELECT * FROM ".$this->tabla;
			$where = " WHERE status='finish'";
			
			if($id_user != 0)
			{
				$where .= " AND group_id IN (SELECT id FROM groups where id_user=".$id_user.")";
			}
			if($from != '')
			{
				$where .= " AND date >= '".$from."'";
			}
			if($to != '')
			{
				$where .= " AND date <= '".$to."'";
			}
			$sql .= $where . " ORDER BY date DESC";
			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Messages = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$newMsg = new Campaign();
				$newMsg->ParseoDeArray($row);
				array_push($Messages,$newMsg);
			}
						
			return $Messages;
		}
		
		function ExportReport($enviados,$noenviados,$group,$sent,$notsent,$output_file_name){
			/** open raw memory as file, no need for temp files, be careful not to run out of memory thought */
			$f = fopen('php://memory', 'w');
			/** loop through array  */	
			$replace = array('•','°','º','á', 'é', 'í', 'ó', 'ú', 'ñ','Á', 'É', 'Í', 'Ó', 'Ú', 'Ñ');
			$find 	 = array('&#8226;','&ordm;','&ordm;','&aacute;', '&eacute;', '&iacute;', '&oacute;', '&uacute;', '&ntilde;','&Aacute;', '&Eacute;', '&Iacute;', '&Oacute;', '&Uacute;', '&Ntilde;');
		
		
			fputcsv($f, array('Campaign Report'));
			fputcsv($f, array('Date: '.$this->date));
			fputcsv($f, array('Type: '.$this->type));
			
			if($this->type == "text"){
				fputcsv($f, array('Message: '.str_ireplace($find,$replace,$this->content)));
			}else{
				$array_contenido = explode('message_record\\',$this->content);
				
				    if($this->type != 'audio'){
	                  	$array_contenido = explode(';',$array_contenido[1]);
	                  	$contenido = $array_contenido[0];
	                  	
	                  	//por si tiene acentos y eso con ;
						$caption = $array_contenido[1]; 
						if(count($array_contenido)>2){
							for($i=2;$i<count($array_contenido);$i++){
								$caption .= ';'.$array_contenido[$i];
							}
						}
						
						fputcsv($f, array('Message: '.URL_ROOT.str_ireplace('\\','/',$contenido)));
						fputcsv($f, array('Caption: '.str_ireplace($find,$replace,$caption)));

	                }else{
						$contenido = $array_contenido[1];
						fputcsv($f, array('Message: '.URL_ROOT.$contenido));
					}
					

			}
			
			
			fputcsv($f, array('Group: '.$group));
			fputcsv($f, array('Successfully sent: '.$sent));
			fputcsv($f, array('Mistakenly sent: '.$notsent));
			
			fputcsv($f, array(''));		
			fputcsv($f, array('Successfully sent detail:'));
				
			foreach ($enviados as $line) {
				/** default php csv handler **/
				fputcsv($f, array($line));
			}
			
			fputcsv($f, array(''));		
			fputcsv($f, array('Mistakenly sent detail:'));
			
			foreach ($noenviados as $line) {
				/** default php csv handler **/
				fputcsv($f, array($line));
			}	

			/** rewrind the "file" with the csv lines **/
			fseek($f, 0);
			/** modify header to be downloadable csv file **/
			header('Content-Type: application/csv');
			header('Content-Disposition: attachement; filename="' . $output_file_name . '";');
			/** Send file to browser for download */
			fpassthru($f);
			
   		 }
		
}
?>