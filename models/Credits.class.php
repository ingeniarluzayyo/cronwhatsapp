<?php
	class Credits {
		
		public $tabla;
		public $id;
		public $name;
		
		public function __construct() {
			$this->tabla 			= 'bulk_messages';
			$this->tabla_usuarios 	= 'usuarios';
			$this->id 				= '';
			$this->name				= '';
		}
		
		public function ParseoDeArray($arr) {
			if(array_key_exists('id', $arr))
			{
				$this->id 			= $arr['id'];
			}
			$this->name				= $arr['name'];

		}
		
		public function setTabla($tabla)
		{
			$this->tabla = $tabla;
		}		
		 		
		public function LogMessage($user,$contact,$state) {		
			$dt = new DateTime();	
			$arr = array(
				'user' 		=> $user,
				'contact' 	=> $contact,
				'state'		=> $state,
				'date' 		=> $dt->format('Y-m-d H:i:s')
				);
			if(!Database::getInstance()->insert($arr, $this->tabla)) {
				return false;
			}
			return true;
		}	
		
		public function DecreaseCredits($user_id,$credits) {
			$sql  = "UPDATE ".$this->tabla_usuarios." SET credits = credits - ".$credits." WHERE id = ".$user_id;			
			if(!Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			return true;
		 }
		 
		 public function GetCreditsSpendedOnMonth_ByUser($id_user,$month) { //1-12
			$sql  = "SELECT count(*) as count FROM ".$this->tabla." WHERE MONTH(date_sent) = ".$month." AND contact_id IN (SELECT id FROM contacts where id_group IN (SELECT id FROM groups WHERE id_user='".$id_user."'))";
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}						
			return $arr['count'];
		}
		
		public function GetCreditsSpendedOnMonth($month) { //1-12
			$sql  = "SELECT count(*) as count FROM ".$this->tabla." WHERE MONTH(date_sent) = ".$month;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}						
			return $arr['count'];
		}
		
	}	

?>