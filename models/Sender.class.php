<?php
class Sender {
		
		public $tabla;
		public $id;
		public $numero;
		public $password;
		public $estado;
		public $enviados;
		public $ultima_actualizacion;
		public $assigned_user;
		public $habilitado;
		
		public function __construct() {
			$this->tabla 				= 'senders';
			$this->id 						= '';
			$this->numero					= '';
			$this->password 				= '';
			$this->estado 					= 'Online';
			$this->enviados 				= '0';
			$this->ultima_actualizacion 	= '';
			$this->assigned_user 			= '0';
			$this->habilitado 				= '';
		}
		
		public function ParseoDeArray($arr) {
			if(array_key_exists('id', $arr))
			{
				$this->id 			= $arr['id'];
			}
			$this->numero			= $arr['numero'];
			$this->password 		= $arr['password'];
			if(array_key_exists('habilitado', $arr))
			{
				$this->habilitado		= $arr['habilitado'];
			}
			if(array_key_exists('estado', $arr))
			{
				$this->estado 			= $arr['estado'];
			}
			if(array_key_exists('enviados', $arr))
			{
				$this->enviados 		= $arr['enviados'];
			}
			if(array_key_exists('ultima_actualizacion', $arr))
			{
				$this->ultima_actualizacion		= $arr['ultima_actualizacion'];
			}
			if(array_key_exists('assigned_user', $arr))
			{
				$this->assigned_user 			= $arr['assigned_user'];
			}
		}
		
			public function setTabla($tabla)
		{
			$this->tabla = $tabla;
		}

		public function Crear() {			
			$dt = new DateTime();	
			$result = $this->ValidarDatos();	
			if($result['state'])
			{
				$arr = array(
					'numero' 				=> DataBase::Cadena($this->numero),
					'password' 				=> DataBase::Cadena($this->password),
					'estado' 				=> DataBase::Cadena($this->estado),
					'enviados' 				=> DataBase::Cadena($this->enviados),
					'ultima_actualizacion' 	=> DataBase::Cadena($dt->format('Y-m-d H:i:s')),
					'assigned_user' 		=> DataBase::Cadena($this->assigned_user),
					'habilitado' 			=> DataBase::Cadena($this->habilitado)
					);
					if(!Database::getInstance()->insert($arr, $this->tabla)) {
					$result['msg'] .= "Error while creating a sender.";
					$result['state'] = false;
					return $result;
				}				
			}else{
				return $result;
			}
			
			$result['msg'] .= "The sender has been created succesfully.<br />";
			return $result;
		}
		
		public function Modificar() {
			$result = $this->ValidarDatos();	
			if($result['state'])
			{
				$arr = array(
					'numero' 		=> DataBase::Cadena($this->numero),
					'password' 		=> DataBase::Cadena($this->password),
					'estado' 		=> DataBase::Cadena($this->estado),
					'habilitado' 	=> DataBase::Cadena($this->habilitado)
					);
				$where = "WHERE id = " . $this->id;
				if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
						$result['msg'] .= "Error while editing a sender.";
						$result['state'] = false;
						return $result;								
				}
			}else{
				return $result;
			}
			
			$result['msg'] .= "The sender has been edited succesfully.<br />";
			return $result;			
		 }
		 
		 public function AssignToUser($user = -1) {
		 	if($user != -1)
		 	{
				$this->assigned_user = $user;
			}
		 	
		 	$dt = new DateTime();
			$arr = array(
					'assigned_user' => $this->assigned_user,
					'ultima_actualizacion'	=> DataBase::Cadena($dt->format('Y-m-d H:i:s'))
					);
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->update($arr, $this->tabla, $where)) {					
					return false;								
			}		

			return true;			
		 }
		 
		 public function Update_available($available = -1) {
			if($available != -1)
			{
				$this->habilitado = $available;
			}
			
			$arr = array(
				'habilitado' 	=> DataBase::Cadena($this->habilitado)
				);
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
					return false;								
			}
		
			return true;		
		 }
		 
		 public function Update_estado($estado = '') {
			if($estado != '')
			{
				$this->estado = $estado;
			}
			
			$arr = array(
				'estado' 	=> DataBase::Cadena($this->estado)
				);
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
					return false;								
			}
		
			return true;		
		 }
		 
		 public function Update_enviados($value) {
		
				$arr = array(
					'enviados' 	=> DataBase::Cadena($this->enviados + $value)
					);
				$where = "WHERE id = " . $this->id;
				if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
						return false;								
				}
		
			return true;		
		 }
		 
		  public function func() {
	        $debug = debug_backtrace();
	        return $debug[2]['function'];
	    } 
		 
		 public function ValidarDatos()
		 {
		 	$result['state'] = true;
		 	$result['msg'] = "";
		 	
		 	if(! ($this->numero != "" AND is_numeric($this->numero)))
		 	{
				$result['state'] = false;
		 		$result['msg'] .= "Invalid characters on number.<br />";
			}
			
			if(($this->password == ""))
		 	{
				$result['state'] = false;
		 		$result['msg'] .= "Password field is required.<br />";
			}
			
			if (self::func() != 'Modificar'){
				$senders = $this->GetbyNumber($this->numero);

				if(count($senders) >= 1)
				{				
					$result['state'] = false;
			 		$result['msg'] .= "The number has alredy exists on the database.<br />";
				}	
			}
					
			return $result;
		 }
		 
		 public function Eliminar() {
		 	$result['state'] = true;
		 	$result['msg'] = "";
		 	
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->delete($this->tabla, $where)) {
				$result['msg'] .= "Error while deleting a sender.";
				$result['state'] = false;
				return $result;
			}
			$result['msg'] .= "The sender has been deleted succesfully.<br />";			
			return $result;	
		 }
		 
		  public function EliminarOffline() {
		 	$result['state'] = true;
		 	$result['msg'] = "";
		 	
			$where = "WHERE estado = 'Offline'";
			if(!Database::getInstance()->delete($this->tabla, $where)) {
				$result['msg'] .= "Error while deleting a senders.";
				$result['state'] = false;
				return $result;
			}
			$result['msg'] .= "The senders has been deleted succesfully.<br />";			
			return $result;	
		 }
			

		 public function newId() {
			$sql  = "SELECT max(id)+1 as id FROM ".$this->tabla;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return 0;
			}			
		
			return $arr["id"];
		}

		public function GetById($id = 0) {
			if($id != 0)
			{
				$this->id = $id;
			}
			
			$sql  = "SELECT * FROM ".$this->tabla." WHERE id = " . $this->id;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}

			$this->ParseoDeArray($arr);			
		
			return true;
		}	
		
		public function GetbyNumber() {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE numero = '".$this->numero."'";			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Contenidos = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$newContenido = new Sender();
				$newContenido->ParseoDeArray($row);
				array_push($Contenidos,$newContenido);
			}
						
			return $Contenidos;
		}
		
		public function GetOnline()
		{
			$sql  = "SELECT * FROM ".$this->tabla. " WHERE habilitado = '1' AND estado = 'Online'";			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Contenidos = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$newContenido = new Sender();
				$newContenido->ParseoDeArray($row);
				array_push($Contenidos,$newContenido);
			}
						
			return $Contenidos;
		}
		
		public function GetbyNumberListen($number) {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE numero LIKE '%".$number."%'";	
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}

			$this->ParseoDeArray($arr);			
		
			return true;
		}
		
		public function GetAll() {
			$sql  = "SELECT * FROM ".$this->tabla. " order by estado desc";			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Contenidos = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$newContenido = new Sender();
				$newContenido->ParseoDeArray($row);
				array_push($Contenidos,$newContenido);
			}
						
			return $Contenidos;
		}
		
		public function GetAvailableIsOnline()
		{
			$sql  = "SELECT * FROM ".$this->tabla. " WHERE (estado='Online' or estado='') AND assigned_user = 0";			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$senders = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$s = new Sender();
				$s->ParseoDeArray($row);
				array_push($senders,$s);
			}
						
			return $senders;
		}
		
		
		public function GetAlltoListen()
		{
			$dt = new DateTime();
			$sql  = "SELECT * FROM ".$this->tabla. " WHERE habilitado = '1' AND assigned_user = 0 AND TIMESTAMPDIFF(HOUR,ultima_actualizacion,'".$dt->format('Y-m-d H:i:s')."') <= 24 ORDER BY ultima_actualizacion DESC";			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$senders = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$s = new Sender();
				$s->ParseoDeArray($row);
				array_push($senders,$s);
			}
						
			return $senders;
		}
		
		public function IsOnline()
		{	
			try
			{
				$wa = new WhatsProt($this->numero,"WhatsApp", false);
				$wa->connect();				
				$wa->loginWithPassword($this->password);
				return true;
			}
			catch(Exception $e) 
			{
				return false;
			}
		}
		
		public function ProfileImg($user)
		{
			$nombre = 'user_'.$user;
			$path = __DIR__.PAIT.'..'.PAIT.'img'.PAIT.'users'.PAIT;
			$foto = ManejoArchivos::existe_foto($nombre,$path);
			$img_sender =  $path.$foto;
			return $img_sender;
		}
		
		public function ParseoCSV($filepath) {
			$senders = array();
			$msg = "";
			$contenido = ManejoArchivos::contenido_archivo($filepath);

			$arr_contenido = explode("\n",$contenido);
			
			$i=1;
			foreach($arr_contenido as $linea)
			{
				$sender = explode(',',$linea);
				if(count($sender) == 2)
				{
					$newContenido = new Sender();
				
					$newContenido->numero 	= $sender[0];
					$newContenido->password 	= $sender[1];
					$newContenido->habilitado 	= 1;
					
					array_push($senders,$newContenido);
				}else{
					$linea = trim($linea);
					if(!empty($linea))
					{
						$msg .= "Error in line $i: ".$linea."<br />";	
					}
				}		
				$i++;		
			}
			
			$result['state'] = ($msg == ""); //No hubo errores
			$result['senders'] = $senders; //Senders extraidos
			$result['msg'] = $msg; //Mensaje de error
						
			return $result;
			
		}
		 
		public function ImportCSV($filepath) {
			$result = $this->ParseoCSV($filepath);
			
			if($result['state'] AND !empty($result['senders']))
			{
				foreach($result['senders'] as $sender)
				{
					$resultado = $sender->Crear();
					$result['state'] = $result['state'] && $resultado["state"];
					if(!$resultado['state'])
					{
						$result['msg'] .= "Error while importing sender: ".$sender->numero."<br />";
					}
				}
			}			
			
			return $result;
			
		}
		
		public function EscuharRespuestas()
		{
			try{						
				$w = new WhatsProt($this->numero, "WhatsApp", false);
				$w->eventManager()->bind("onGetMessage", "onMessage");			
				
				$w->Connect();			
				$w->LoginWithPassword($this->password);
				
				while($w->PollMessage());
			}catch(LoginFailureException $e_l){
				echo "[Listen]Login Faild Error <br />";
				//$this->habilitado = 0;
				//$this->Update_available();							
			}catch(Exceptio $e){
				//Para romper el bucle
			}					
		}
		
		public function UpdateEnviados($value) {		
			$result['state'] = true;
			$result['msg'] 	 = "";
			
		 	if(is_numeric($value))
		 	{								
				$arr = array(
					'enviados' 		=> ($this->enviados + $value)			
					);
				$where = "WHERE id = " . $this->id;
				if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
					$result['msg'] .= "Error while updating enviados.";
					$result['state'] = false;
					return $result;
				}
				$result['msg'] .= "The sender has been cleaned succesfully.<br />";					
				return $result;
			}
			$result['msg'] .= "Invalid character.";
			$result['state'] = false;
			return $result;	
		 }
		 
		public function HaveEnougthSenders($id_user)
		{
			$sql  = "SELECT * FROM ".$this->tabla. " WHERE estado='Online' AND habilitado = '1' AND assigned_user = 0 AND enviados < ".CANT_MAX_A_ENVIAR_POR_SENDER." ORDER BY ultima_actualizacion,enviados LIMIT 0,1";			
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}

			$s = new Sender();
			$s->ParseoDeArray($arr);

			$s->assigned_user = $id_user;
			return ($s->AssignToUser());	
		}
		
		public function GetSenderAvailable($user)
		{
			//Pido los datos del nuevo sender
			$sql  = "SELECT * FROM ".$this->tabla. " WHERE estado='Online' AND habilitado = '1' AND assigned_user = ".$user." AND enviados < ".CANT_MAX_A_ENVIAR_POR_SENDER." ORDER BY ultima_actualizacion,enviados ASC LIMIT 0,1";						
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				$s = new Sender();
				
				return $s;				
			}			
			
			$s = new Sender();
			$s->ParseoDeArray($arr);
			return $s;			
		}
		
		public function GetSenderAvailableToSyncronize($user)
		{
			//Pido los datos del nuevo sender
			$sql  = "SELECT * FROM ".$this->tabla. " WHERE habilitado = '1' AND assigned_user = 0 AND estado = 'Online' ORDER BY ultima_actualizacion ASC LIMIT 0,1";						
			
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				$s = new Sender();
				
				return $s;				
			}			
			
			$s = new Sender();
			$s->ParseoDeArray($arr);
			$s->AssignToUser($user);
			return $s;			
		}
}

function onMessage($mynumber, $from, $id, $type, $time, $name, $body)
{	
	if($body != "")
	{			
		$numero = $number = explode('@',$from);
		$numero = $numero[0];
		
		$sender_number = $sender_number = explode('@',$mynumber);
		$sender_number = $sender_number[0];
		
		$fecha = new DateTime();	
		$fecha->setTimestamp($time);
		
		$sender = new Sender();
		$listen = new ListenSent();
		$sender->GetbyNumberListen($sender_number);
		
		$listen->contenido = $body;
		$listen->numero = $numero;
		$listen->sender = $sender->id;
		$listen->cluster = 0;
		$listen->fecha = $fecha->format('Y-m-d H:i:s');
		
		echo "[".$listen->numero."][".$listen->contenido."] -> Sender:[".$listen->sender."]<br />";
		
		$listen->Crear();
	}
}
?>