<?php
	class Group {
		
		public $tabla;
		public $id;
		public $id_user;
		public $name;
		public $cant;
		
		public function __construct() {
			$this->tabla 			= 'groups';
			$this->tabla_contacts	= 'contacts';
			$this->id 				= '';
			$this->id_user			= '';
			$this->name				= '';
			$this->cant				= '';
		}
		
		public function ParseoDeArray($arr) {
			if(array_key_exists('id', $arr))
			{
				$this->id 			= $arr['id'];
			}
			if(array_key_exists('id_user', $arr))
			{
				$this->id_user			= $arr['id_user'];
			}
			$this->name				= $arr['name'];
			
			if(array_key_exists('cant', $arr))
			{
				$this->cant			= $arr['cant'];
			}
		}
		
		public function setTabla($tabla)
		{
			$this->tabla = $tabla;
		}

		public function Crear() {	
			$result = $this->ValidarDatos();	
			if($result['state'])
			{		
				$arr = array(
					'id_user' 	=> $this->id_user,
					'name' 		=> DataBase::Cadena($this->name)
					);
				if(!Database::getInstance()->insert($arr, $this->tabla)) {
					$result['msg'] .= "Error while creating a group.";
					$result['state'] = false;
					return $result;
				}				
			}else{
				return $result;
			}
			
			$result['msg'] .= "The group has been created succesfully.<br />";
			return $result;
		 }
		 
		public function Modificar() {
			$result = $this->ValidarDatos();	
			if($result['state'])
			{
				$arr = array(
					'id_user' 	=> $this->id_user,
					'name' 		=> DataBase::Cadena($this->name)
					);
				$where = "WHERE id = " . $this->id;
				if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
					$result['msg'] .= "Error while editing a group.";
					$result['state'] = false;
					return $result;
				}
				
			}else{
				return $result;
			}
			
			$result['msg'] .= "The group has been edited succesfully.<br />";
			return $result;
		 }
		
		public function ValidarDatos()
		 {
		 	$result['state'] = true;
		 	$result['msg'] = "";
		 	
		 	if(! ($this->name != "" AND is_string($this->name)))
		 	{
				$result['state'] = false;
		 		$result['msg'] .= "Invalid characters on name.<br />";
			}
			if(! ($this->id_user != "" AND is_numeric($this->id_user)))
		 	{
				$result['state'] = false;
		 		$result['msg'] .= "Error asociating the group to the user.<br />";
			}			
						
			return $result;
		 }
		 
		public function Eliminar() {
			$result['state'] = true;
			$result['msg'] 	 = "";
			
			$where = "WHERE id_group = " . $this->id;
				if(!Database::getInstance()->delete($this->tabla_contacts, $where)) {
					$result['msg'] .= "Error while deleting the contacts of the group.";
					$result['state'] = false;
					return $result;
			}
			
			
			if($result['state'])
			{							
				$where = "WHERE id = " . $this->id;
				if(!Database::getInstance()->delete($this->tabla, $where)) {
					$result['msg'] .= "Error while deleting a group.";
					$result['state'] = false;
					return $result;
				}
			}
			
			$result['msg'] .= "The group has been deleted succesfully.<br />";			
			return $result;			
		 }

		 public function newId() {
			$sql  = "SELECT max(id)+1 as id FROM ".$this->tabla;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return 0;
			}			
		
			return $arr["id"];
		}

		public function GetById($id,$id_user = 0) {
			if($id_user != 0 ){
				$sql  = "SELECT * FROM ".$this->tabla." WHERE id = " . $id." AND id_user = ".$id_user;
			}else{
				$sql  = "SELECT * FROM ".$this->tabla." WHERE id = " . $id;
			}
			
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}

			$this->ParseoDeArray($arr);			
		
			return true;
		}		

		public function GetAll() {
			$sql  = "SELECT * FROM ".$this->tabla." ORDER BY name";			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Contenidos = array();
			
			//$oUsuario->ParseoDeArray($arr);
			while($row = mysqli_fetch_array($arr))
			{
				$newContenido = new Group();
				$newContenido->ParseoDeArray($row);
				array_push($Contenidos,$newContenido);
			}
						
			return $Contenidos;
		}
		
		public function GetAllOfUser($id_user) {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE id_user = ".$id_user." ORDER BY name";			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Contenidos = array();
			
			//$oUsuario->ParseoDeArray($arr);
			while($row = mysqli_fetch_array($arr))
			{
				$newContenido = new Group();
				$newContenido->ParseoDeArray($row);
				array_push($Contenidos,$newContenido);
			}
						
			return $Contenidos;
		}	
		
		public function GetArrGroups() {
			$sql  = "SELECT * FROM ".$this->tabla." ORDER BY name";			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Contenidos = array();						
			
			while($row = mysqli_fetch_array($arr))
			{				
				$Contenidos[$row["id"]] = $row["name"];
			}
						
			return $Contenidos;
		}	
		
		public function GetArrGroupsOfUser($id_user) {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE id_user = ".$id_user." ORDER BY name";
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Contenidos = array();						
			
			while($row = mysqli_fetch_array($arr))
			{				
				$g = new Group();
				$g->GetById($row["id"],$id_user);
				$Contenidos[$row["id"]] = $row["name"]." (".$g->GetCantContacts().")";
			}
						
			return $Contenidos;
		}
		
		public function GetCantContacts() {
			$sql  = "SELECT cant FROM ".$this->tabla." WHERE id= " . $this->id;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return 0;
			}

			return $arr['cant'];								
		}
		
	}	

?>