<?php
include_once("lib/variables.php");
require_once("lib/clases.php");
include_once("lib/textos_estaticos.php");
session_start();

LoginIngeniar::VerificarLogin();

if(isset($_GET['logout']) && $_GET['logout']==1)
{				
	LoginIngeniar::Logout();
}

//Tomar apartado a seleccionar por POST o GET
if(isset($_GET["apartado"]))
{
	$apartado = $_GET["apartado"];
}

if(isset($_POST["apartado"]))
{
	$apartado = $_POST["apartado"];
}

//Control de Pie y Menu
require_once("controls/body/body_control.php");

if(isset($apartado))
{	
	$seccion_seleccionada = $apartado;
	
	switch($apartado)
	{
		case "home":
		{
			require_once("controls/home/home_control.php");
			break;
		}
		
		case "settings":
		{
			if($user->IsAdmin($_SESSION['id_user']))
			{
				require_once("controls/settings/settings_control.php");
			}else
			{
				$seccion_seleccionada = 'home';
				require_once("controls/home/home_control.php");
			}
			break;
		}
		
		case "registration":
		{
			if($user->IsAdmin($_SESSION['id_user']))
			{
				require_once("controls/registration/registration_control.php");
			}
			break;
		}
		case "registration_sender":
		{
			if($user->IsAdmin($_SESSION['id_user']))
			{
				require_once("controls/registration/registration_sender_control.php");
			}
			break;
		}
		
		//ABM Proxy
		case "newproxy":
		{		
			if($user->IsAdmin($_SESSION['id_user']))
			{
				require_once("controls/proxy/newProxy_control.php");
			}else
			{
				$seccion_seleccionada = 'home';
				require_once("controls/home/home_control.php");
			}
			break;
		}
		
		case "modproxy":
		{
			if($user->IsAdmin($_SESSION['id_user']))
			{
				require_once("controls/proxy/modProxy_control.php");
			}else
			{
				$seccion_seleccionada = 'home';
				require_once("controls/home/home_control.php");
			}
			break;
		}
		
		
		case "error_send":
		{
			if($user->IsAdmin($_SESSION['id_user']))
			{
				require_once("controls/error_send/error_send_control.php");
			}else
			{
				$seccion_seleccionada = 'home';
				require_once("controls/home/home_control.php");
			}
			break;
		}
		
		case "settings_user":
		{
			require_once("controls/settings/settings_user_control.php");
			break;
		}
		
		case "listen":
		{
			require_once("controls/listen/modListen_control.php");
			break;
		}
		
		case "singlemessage":
		{
			require_once("controls/singlemessage/singlemessage_control.php");
			break;
		}
		
		case "bulkmessage":
		{
			require_once("controls/bulkmessage/bulkmessage_control.php");
			break;
		}
		
		//Messages
		case "msgsended":
		{
			require_once("controls/messages/lstMessagesSended_control.php");
			break;
		}
		
		//Groups and CONTACTS
		case "newgroup":
		{
			require_once("controls/groups/newGroup_control.php");
			break;
		}
		case "modgroup":
		{
			require_once("controls/groups/modGroup_control.php");
			break;
		}
		case "importgroup":
		{
			require_once("controls/groups/importContacts_control.php");
			break;
		}
		case "newcontact":
		{
			require_once("controls/groups/newContact_control.php");
			break;
		}
		case "modcontact":
		{
			require_once("controls/groups/modContact_control.php");
			break;
		}
			
		//Usuarios
		case "newuser":
		{
			if($user->IsAdmin($_SESSION['id_user']))
			{
				require_once("controls/usuarios/newUser_control.php");
			}else
			{
				$seccion_seleccionada = 'home';
				require_once("controls/home/home_control.php");
			}
			break;
		}
		case "moduser":
		{
			if($user->IsAdmin($_SESSION['id_user']))
			{
				require_once("controls/usuarios/modUser_control.php");
			}else
			{
				$seccion_seleccionada = 'home';
				require_once("controls/home/home_control.php");
			}
			break;
		}
		
		//Senders
		case "newsender":
		{
			if($user->IsAdmin($_SESSION['id_user']))
			{
				require_once("controls/senders/newSender_control.php");
			}else
			{
				$seccion_seleccionada = 'home';
				require_once("controls/home/home_control.php");
			}
			break;
		}
		case "modsender":
		{
			if($user->IsAdmin($_SESSION['id_user']))
			{
				require_once("controls/senders/modSender_control.php");
			}else
			{
				$seccion_seleccionada = 'home';
				require_once("controls/home/home_control.php");
			}
			break;
		}
		
		case "importsender":
		{
			if($user->IsAdmin($_SESSION['id_user']))
			{
				require_once("controls/senders/importSender_control.php");
			}else
			{
				$seccion_seleccionada = 'home';
				require_once("controls/home/home_control.php");
			}
			break;
		}
		
		//Package
		case "packages":
		{
			require_once("controls/packages/Package_control.php");

			break;
		}
		
		case "pendingcredits":
		{
			if($user->IsAdmin($_SESSION['id_user']))
			{
				require_once("controls/packages/PendingCredits_control.php");
			}else
			{
				$seccion_seleccionada = 'home';
				require_once("controls/home/home_control.php");
			}
			break;
		}
		
		case "newpackage":
		{
			if($user->IsAdmin($_SESSION['id_user']))
			{
				require_once("controls/packages/newPackage_control.php");
			}else
			{
				$seccion_seleccionada = 'home';
				require_once("controls/home/home_control.php");
			}
			break;
		}
		case "modpackage":
		{
			if($user->IsAdmin($_SESSION['id_user']))
			{
				require_once("controls/packages/modPackage_control.php");
			}else
			{
				$seccion_seleccionada = 'home';
				require_once("controls/home/home_control.php");
			}
			break;
		}
			
		case "404":
		{
			require_once("controls/404/404_control.php");
			break;
		}

		default:
		{
			$seccion_seleccionada = 'home';
			require_once("controls/home/home_control.php");
			break;
		}
	}
}else{
	$seccion_seleccionada = 'home';
	require_once("controls/home/home_control.php");	
}





?>
