<?php
/************************
	
Nombre: Global
Version: 1.0	
Fecha de creacion: 11/10/2014
Autor: Martin
Fecha de ultima modificacion: 11/10/2014
Autor de ultima modificacion: Martin


DESCRIPCION:
Define el directorio raiz en el cual se encuentra el Proyecto e incluye los archivos esenciales para controlar que el usuario este logeado.

Se lo suele utilizar al inicio de los controles de las secciones privadas para control de acceso.
	
*************************/
	
define('__ROOT__', dirname(dirname(__FILE__)));

include_once(__ROOT__."/lib/variables.php");
include_once(__ROOT__."/lib/class/LoginIngeniar.class.php");

LoginIngeniar::Denegar($url_root);

?>