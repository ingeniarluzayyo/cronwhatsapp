<?php
/************************
	
Nombre: Variables
Version: 1.0	
Fecha de creacion: 11/10/2014
Autor: Martin
Fecha de ultima modificacion: 11/10/2014
Autor de ultima modificacion: Martin


DESCRIPCION:
Contiene la definicion de las rutas basicas de funcionamiento de la web.
	
*************************/


$os = php_uname("s");
(is_numeric(stripos($os,'windows')))?$pait = "\\\\":$pait="/";

$hd_root = str_replace("\\",$pait,getcwd()).$pait;
$hd_imagenes = $hd_root . "img".$pait;
$hd_imagenes_adm = $hd_root . "..".$pait."img".$pait;
$hd_documentos = $hd_root . "documentos".$pait;

$url_root = getBaseUrl();
$url_imagenes = $url_root . "img/";
$url_imagenes_adm = $url_root . "../img/";
$url_documentos = $url_root . "documentos/";

define('HD_IMAGES',$hd_imagenes);
define('URL_IMAGES',$url_imagenes);
define('URL_ROOT',$url_root);
define('PAIT',$pait);

function getBaseUrl()
{
    // output: /myproject/index.php
    $currentPath = $_SERVER['PHP_SELF'];
     
    // output: Array ( [dirname] => /myproject [basename] => index.php [extension] => php [filename] => index )
    $pathInfo = pathinfo($currentPath);
     
    // output: localhost
    $hostName = $_SERVER['HTTP_HOST'];
     
    // output: http://
    $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https://'?'https://':'http://';
     
    // return: http://localhost/myproject/
    return $protocol.$hostName.$pathInfo['dirname']."/";
}
?>