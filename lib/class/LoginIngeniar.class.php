<?php
define('HD_ROOT',getcwd()."/");

class LoginIngeniar
{
	/************************
	
	Nombre: Login
	Version: 1.0	
	Fecha de creacion: 11/10/2014
	Autor: Martin
	Fecha de ultima modificacion: 11/10/2014
	Autor de ultima modificacion: Martin


	METODOS:

	Login::Denegar($HD_ROOT); ->Si no esta logeado redirige al index.php del $HD_ROOT
	Login::EstaLogeado(); ->Verifica si la cookie de session esta seteada correctamente
	Login::Logout(); -> Borra la cookie de session y destruye la session de PHP, luego redirige a la misma pagina (la cual debe Denegar el acceso y redirigir a otra)
	Login::Loguearse($username,$password); -> Autentica al usuario y password con la tabla de la BD, setea la cookie y la session
	Login::VerificarLogin(); ->Verifica si la cookie de session esta seteada correctamente, si no lo esta redirige al Login
	
	cookie_string_password: Es el password de la cookie de session
	cookie_string_session: Es el nombre de la cookie de session
	
	*************************/
	
	public static $cookie_string_session = "fewf3454vvr4kilojewofg4";
	public static $cookie_string_password = "ewwqn42h32ef3f3h78hfewgh";


	public static function VerificarLogin()
	{
		//!(isset($_COOKIE[self::$cookie_string_session]) && ($_COOKIE[self::$cookie_string_session] == self::$cookie_string_password) ) OR 
		if(
		!(isset($_COOKIE[self::$cookie_string_session]) && ($_COOKIE[self::$cookie_string_session] == self::$cookie_string_password))
		OR
		!(isset($_SESSION['id_user']))
		OR
		!(isset($_SESSION['username']))
		)
		{					
			if(isset($_GET['login']) && $_GET['login']==1)
			{				
				require_once(HD_ROOT."controls/login/login_control.php");
				exit();					
			}else{
				require_once(HD_ROOT."views/login/login.phtml");
				exit();
			}
		}
		
		
	}

	public static function Denegar($HD_ROOT)
	{		
		if(!LoginIngeniar::EstaLogeado())
		{	
			header("Location: ".$HD_ROOT."index.php");
		}
	}
	public static function Logout()
	{
		unset($_COOKIE[self::$cookie_string_session]);	
		setcookie(self::$cookie_string_session,'');				
		session_destroy();		
		header("Location: ?");
		exit();
	}

	public static function Loguearse($username,$password)
	{	
		$query = "SELECT * FROM usuarios WHERE habilitado = '1' AND username = '".$username."' AND password = '".md5($password)."'";		
		if($arr = Database::getInstance()->GetUnicoQuery($query))
		{			
			setcookie(self::$cookie_string_session,self::$cookie_string_password);
			session_start();
			$_SESSION['id_user']=$arr['id'];
			$_SESSION['username']=$username;			
			return true;
		
		}			
		return false;
	}

	public static function EstaLogeado()
	{
		return (isset($_COOKIE[self::$cookie_string_session]) && ($_COOKIE[self::$cookie_string_session] == self::$cookie_string_password) && isset($_SESSION['username']) && isset($_SESSION['id_user']) );
		
	}
}

?>