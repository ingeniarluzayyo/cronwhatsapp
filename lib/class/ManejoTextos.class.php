<?php

class ManejoTextos
{
	/************************
	
	Nombre: ManejoTextos
	Version: 1.0	
	Fecha de creacion: 11/10/2014
	Autor: Martin
	Fecha de ultima modificacion: 11/10/2014
	Autor de ultima modificacion: Martin


	METODOS:

	ManejoTextos::cambiaFecha_a_normal($fecha); ->Cambia la fecha del formato '2000-01-01' >> '01/01/2000'
	ManejoTextos::cambiaFecha_a_mysql($fecha); ->Cambia la fecha del formato '01/01/2000' >> '2000-01-01' 
	
		
	*************************/
	
	public static function html2txt($txt)
	{
		return htmlentities($txt);
	}

	public static function txt2html($txt)
	{
		 return html_entity_decode($txt);
	}
	
	public static function cambiaFecha_a_normal($fecha){	    
	    $date = new DateTime($fecha);//'2000-01-01'
		return $date->format('d/m/Y');
	}

	public static function cambiaFecha_a_mysql($fecha){
		$fecha = str_replace("/","-",$fecha);
	    $date = new DateTime($fecha);//'2000-01-01'
		return $date->format('Y-m-d');
	} 
	
	public static function UTF8_esp($texto)
	{
		$find = array('•','°','º','á', 'é', 'í', 'ó', 'ú', 'ñ','Á', 'É', 'Í', 'Ó', 'Ú', 'Ñ');
		$replace = array('Â·','Â°','Â°','Ã¡', 'Ã©', 'Ã­', 'Ã³', 'Ãº', 'Ã±','Ã', 'Ã‰', 'Ã', 'Ã“', 'Ãš', 'Ã‘');
		$replace = array('Â·','Â°','Â°','Ã¡', 'Ã©', 'Ã­', '&oacute;', 'Ãº', '&ntilde;','Ã', 'Ã‰', 'Ã', 'Ã“', 'Ãš', 'Ã‘');
		
		return str_ireplace($find,$replace,$texto);
	}
}

?>