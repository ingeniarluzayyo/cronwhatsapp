<?php

class Tablas
{
	/************************
	
	Nombre: Tablas
	Version: 1.0	
	Fecha de creacion: 11/10/2014
	Autor: Martin
	Fecha de ultima modificacion: 11/10/2014
	Autor de ultima modificacion: Martin


	METODOS:

	Tablas::agregarFila(array("columna 1","columna 2","columna 3","columna 4")); ->agrega una fila a la tabla poniendo contenido a cada columna de la tabla
	Tablas::agregarFilaForm($action,$method,array("columna 1","columna 2","columna 3","columna 4")); ->agrega una fila a la tabla  la cual es todo un formulario poniendo contenido a cada columna de la tabla
	Tablas::cabecera($titulo); ->Le agrega un encabezado h2 a la tabla (Hay que hacerlo al inicio)
	Tablas::cabecera(array("columna 1","columna 2","columna 3","columna 4")); ->Crea el inicio de la tabla con la cantidad de columnas del array parametro y su contenido.
		
	$tabla: Contiene todo el codigo html de la tabla que se esta generando.
		
	*************************/
	
	public $tabla;

	public function __construct() {
			$this->tabla 			= '';
		}	

	public function generarTabla()
	{
		echo $this->tabla;
	}		
	public function cabecera($titulo)
	{
		$this->tabla .= '';
	}

	public function inicioTabla($columnas)//ej parametro: array("Número","Usuario","Habilitado","Acciones")
	{
		$this->tabla .= '
						<div class="table-responsive">
			                <table class="table table-bordered table-hover tablesorter">
			                  <thead>
			                    <tr>
			             		';

		foreach($columnas as $columna)
		{
			$this->tabla .= '<th>'.$columna.'</th>';			                      		
		}
		$this->tabla .=	'   </tr>
			                  </thead>
			                  <tbody>
							';
	}

	public function agregarFila($columnas)
	{
		$this->tabla .='<tr>';

		foreach($columnas as $columna)
		{	
			$this->tabla .='
					
                      <td>'
                      .$columna.
                      '</td>';
			             						                      	
		}	
		$this->tabla .='</tr>';	
	}

	public function agregarFilaForm($action,$method,$columnas)//Sirve para la adm
	{
		$this->tabla .=' <form action="'.$action.'" method="'.$method.'">
							<tr>';

		foreach($columnas as $columna)
		{	
			$this->tabla .='
					
                      <td>'
                      .$columna.
                      '</td>';
			             						                      	
		}	
		$this->tabla .='	</tr>
						</form>';	
	}

}

?>