<?php
define('__SELF_DIR__', dirname(dirname(__FILE__)));
include_once(__SELF_DIR__."../../lib/global.php");

require_once("lib/api/whatsapp/whatsprot.class.php");
require_once("models/Sender.class.php");
require_once("models/Proxy.class.php");
$sender = new Sender();
$proxy = new Proxy();

$proxy_arr = $proxy->GetArr();


if(isset($_POST["aceptar"]) && isset($_POST["numero"]) && is_numeric($_POST["numero"]))
{	
	

	try{			
		$w = new WhatsProt($_POST["numero"], "", false);
		
		if(isset($_POST['proxy'])){
			$proxy->GetById($_POST["proxy"]);
			
			if($proxy->id != ""){
				$result_request = $w->codeRequest('sms',$proxy);
			}else{
				$result_request = $w->codeRequest('sms');
			}
			
		}else{
			$result_request = $w->codeRequest('sms');
		}
		
		if($result_request != '' ){
			if($result_request->status == 'sent')
			{
				$result['state'] = true;
				$result['msg'] = "The code has been requested succesfully.";
			}else{
				$result['state'] = false;
				$result['msg'] = "An error happends when requesting the code.<br />Status: ".$result_request->status;	
			}
						
		}else{
			$result['state'] = false;
			$result['msg'] = "An error happends when requesting the code.<br />Check proxy type remember that you need a Elite Proxy.";	
		}
		
	}
	catch(Exception $e)
	{
		$result['state'] = false;
		$result['msg'] = "An error happends when requesting the code.<br />".$e->getMessage();	
	}
}

if(isset($_POST["regitrar"]) && isset($_POST["numero"]) && isset($_POST["password"]))
{	
	$sender = new Sender();
	$sender->ParseoDeArray($_POST);
	
	$result = $sender->Crear();
}

require_once("views/registration/registration_view.phtml");

?>
