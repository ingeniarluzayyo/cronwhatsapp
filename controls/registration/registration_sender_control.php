<?php
define('__SELF_DIR__', dirname(dirname(__FILE__)));
include_once(__SELF_DIR__."../../lib/global.php");

require_once("lib/api/whatsapp/whatsprot.class.php");
require_once("models/Sender.class.php");
require_once("models/Proxy.class.php");

$sender = new Sender();
$proxy = new Proxy();

$proxy_arr = $proxy->GetArr();


if(isset($_POST["aceptar"]) && isset($_POST["numero"]) && isset($_POST["code"]))
{	
	try{			
		$w = new WhatsProt($_POST["numero"], "", false);
		$_POST["code"] = str_replace("-","",$_POST["code"]);
		
		if(isset($_POST['proxy'])){
			$proxy->GetById($_POST["proxy"]);
			
			if($proxy->id != ""){
				$response = $w->codeRegister($_POST["code"],$proxy);
			}else{
				$response = $w->codeRegister($_POST["code"]);
			}
			
		}else{
			$response = $w->codeRegister($_POST["code"]);
		}
		
		if($response != ''){
			$sender = new Sender();
			$sender->numero = $response->login;
			$sender->password = $response->pw;
			$sender->estado = 'Online';
			$sender->habilitado = '1';
			
			$result = $sender->Crear();
		}else{
			$result['state'] = false;
			$result['msg'] = "An error happends when registring a sender.<br />Check proxy type remember that you need a Elite Proxy.";	
		}
		
	}catch(Exception $e)
	{
		$result['state'] = false;
		$result['msg'] = "An error happends when registring a sender.<br />".$e->getMessage();	
	}
	
	
}

require_once("views/registration/registration_sender_view.phtml");

?>
