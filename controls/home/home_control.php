<?php
//Llamar al modelo
require_once("models/Usuario.class.php");
require_once("models/Credits.class.php");
require_once("models/Group.class.php");
require_once("models/Sender.class.php");
require_once("models/Campaign.class.php");
require_once("models/Bulk_Messages.class.php");
require_once("models/Packages_x_User.class.php");


$user = new Usuario();
$credits = new Credits();
$senders = new Sender();
$campaign = new Campaign();
$message = new Bulk_Messages();
$group = new Group();

$packages_x_user = new Packages_x_User();

//Manejo de datos
$user->GetById($_SESSION['id_user']);
$credits_send = $credits->GetCreditsSpendedOnMonth_ByUser($_SESSION['id_user'],date("n"));


if(isset($_POST['id']))
{
	$campaign->id = addslashes($_POST['id']);

	if(isset($_POST["pause"]))
	{
		$campaign->status='paused';
		$campaign->Update_Status();
	}
	
	if(isset($_POST["start"]))
	{
		$campaign->status='waiting';
		$campaign->Update_Status();
	}
}

if(isset($_GET['accion']) && $_GET['accion'] == 'borrar' && isset($_GET['id']) && is_numeric($_GET['id']))
{
	$campaign->GetById($_GET['id']);
	if($campaign->id != "")
	{
		$group->GetById($campaign->group_id);
		if($group->id_user == $_SESSION['id_user'] || $user->IsAdmin($_SESSION['id_user']))
		{
			$message->campaign_id = $_GET['id'];
			if($message->Eliminar())
			{
				$result = $campaign->Eliminar();
			}
		}
	}	
}

if($user->IsAdmin($_SESSION['id_user'])){
	$campaign = $campaign->GetQueues();
}else{
	$campaign = $campaign->GetQueues($_SESSION['id_user']);
}



//Llamar a la vista
require_once("views/home/home_view.phtml");
?>