<?php
define('__SELF_DIR__', dirname(dirname(__FILE__)));
include_once(__SELF_DIR__."../../lib/global.php");

//Llamar a al modelo
require_once("models/Bulk_Messages.class.php");
require_once("models/Single_Messages.class.php");
require_once("models/Campaign.class.php");
require_once("models/Group.class.php");
require_once("models/Contact.class.php");
require_once("models/Usuario.class.php");

$bulk_message = new Bulk_Messages();
$single_message = new Single_Messages();
$usuario = new Usuario();
$campaign = new Campaign();
$group = new Group();
$contact = new Contact();

$from = '';
$to = '';
$users_messages = $usuario->GetAll();
$type = (isset($_GET['type'])? $_GET['type']:'');
$user_messages = ($usuario->IsAdmin($_SESSION['id_user']))?0:$_SESSION['id_user'];

if(isset($_POST["search"]))
{
	$type = (isset($_POST['type'])? $_POST['type']:'');
	$from = (isset($_POST['from'])? $_POST['from']:'');
	$to   = (isset($_POST['to'])? $_POST['to']:'');
	if($usuario->IsAdmin($_SESSION['id_user'])){
		$user_messages = (isset($_POST['user'])? $_POST['user']:'');
	}	
}

if(isset($_GET['accion']) && $_GET['accion'] == 'borrarbulk' && isset($_GET['id']) && is_numeric($_GET['id']))
{
	$type = 'bulk';
	
	$campaign->GetById($_GET['id']);
	if($campaign->id != "")
	{
		$group->GetById($campaign->group_id);
		if($group->id_user == $_SESSION['id_user'] || $usuario->IsAdmin($_SESSION['id_user']))
		{
			$bulk_message->campaign_id = $_GET['id'];
			if($bulk_message->Eliminar())
			{
				$result = $campaign->Eliminar();
			}
		}
	}	
}

if(isset($_GET['accion']) && $_GET['accion'] == 'borrarsingle' && isset($_GET['id']) && is_numeric($_GET['id']))
{
	$type = 'single';

	if($single_message->user == $_SESSION['id_user'] || $usuario->IsAdmin($_SESSION['id_user']))
	{
		$single_message->id = $_GET['id'];
		$result = $single_message->Eliminar();		
	}
		
}

if(isset($_GET['accion']) && $_GET['accion'] == 'export' && isset($_GET['id']) && is_numeric($_GET['id']))
{
	$campaign->GetById($_GET['id']);
	$group->GetById($campaign->group_id);	
	$bulk_message->campaign_id=$campaign->id;
	
	$enviados = $bulk_message->GetAll(1);
	$array_enviados = array();
	$array_noenviados = array();
	
	foreach($enviados as $e){
		$contact->GetById($e->contact_id);		
		array_push($array_enviados,$contact->name.','.$contact->number);	
	}
	
	$noenviados = $bulk_message->GetAll(0);
	foreach($noenviados as $e){
		$contact->GetById($e->contact_id);		
		array_push($array_noenviados,$contact->name.','.$contact->number);	
	}
	
	$campaign->ExportReport($array_enviados,$array_noenviados,$group->name,$bulk_message->CountSent(),$bulk_message->CountNotsent(),$campaign->date.".txt");	
	exit;
}

if($type == 'bulk'){

	$campaigns = $campaign->Search($user_messages,$from,$to);
	//Llamar a la vista
	if($usuario->IsAdmin($_SESSION['id_user'])){
		require_once("views/messages/lstBulkMessagesSended_Admin_view.phtml");	
	}else{
		require_once("views/messages/lstBulkMessagesSended_view.phtml");	
	}
	exit;
}

if($type == 'single'){
	$single_messages = $single_message->Search($user_messages,$from,$to);
	//Llamar a la vista
	if($usuario->IsAdmin($_SESSION['id_user'])){
		require_once("views/messages/lstSingleMessagesSended_Admin_view.phtml");	
	}else{
		require_once("views/messages/lstSingleMessagesSended_view.phtml");	
	}
	exit;
}

require_once("controls/home/home_control.php");	

?>