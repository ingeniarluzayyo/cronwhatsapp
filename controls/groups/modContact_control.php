<?php
define('__SELF_DIR__', dirname(dirname(__FILE__)));
include_once(__SELF_DIR__."../../lib/global.php");

//Llamar a al modelo
require_once("models/Group.class.php");
require_once("models/Contact.class.php");
$group = new Group();
$contact = new Contact();

$name = '';
$id_group = '';
$number = '';
$whatsapp_install = '';	
//Procesamiento de GET
$id_group = 0;
$pagina = 0;
$start = -1;
$requested_url =  preg_replace("/&pag=[0-9][0-9]*/","",$_SERVER['REQUEST_URI']);

if(isset($_GET['accion']) && $_GET['accion'] == 'borrar' && isset($_GET['id']) && is_numeric($_GET['id']))
{
	$contact->GetById($_GET['id'],$_SESSION['id_user']);
	$result = $contact->Eliminar();
}

if(isset($_GET['accion']) && $_GET['accion'] == 'delete_wa_no')
{	
	$result = $contact->Eliminar_WA_Install_No($_SESSION['id_user']);
}


$arr_grupos = $group->GetArrGroupsOfUser($_SESSION['id_user']);

if(isset($_GET['pag']) AND is_numeric($_GET['pag']))
{
	$pagina = $_GET['pag']-1;
	if($pagina < 0){$pagina=0;}		
}

$start = ($pagina+1) * CANTIDAD_ITEMS_PAGINA - CANTIDAD_ITEMS_PAGINA;

if(isset($_POST["search"]) || isset($_GET["search"]))
{
	$name = isset($_POST["name"])?$_POST["name"]:'';
	$id_group = isset($_POST["group"])?$_POST["group"]:'';
	$number = isset($_POST["number"])?$_POST["number"]:'';
	$whatsapp_install = isset($_POST["whatsapp_install"])?$_POST["whatsapp_install"]:'';
	
	$name = isset($_GET["name"])?$_GET["name"]:$name;
	$id_group = isset($_GET["group"])?$_GET["group"]:$id_group;
	$number = isset($_GET["number"])?$_GET["number"]:$number;
	$whatsapp_install = isset($_GET["whatsapp_install"])?$_GET["whatsapp_install"]:$whatsapp_install;

	$requested_url .= "?apartado=".$apartado."&search=1";
	if($name != ''){$requested_url .= "&name=".$name;}
	if($id_group != ''){$requested_url .= "&group=".$id_group;}
	if($number != ''){$requested_url .= "&number=".$number;}
	if($whatsapp_install != ''){$requested_url .= "&whatsapp_install=".$whatsapp_install;}
		
	switch($whatsapp_install)
	{
		case 1:$whatsapp_install= "Yes";break;
		case 2:$whatsapp_install= "No";break;
		case 3:$whatsapp_install= "Not yet verified";break;		
	}	
	
	$total = $contact->SearchContactTotal($whatsapp_install,$name,$number,$id_group,$_SESSION['id_user']);
	$contacts = $contact->SearchContact($whatsapp_install,$name,$number,$id_group,$_SESSION['id_user'],$start);
	require_once("views/groups/lstContact_view.phtml");	
	exit;
}

if($id_group == 0)
{
	$total = $contact->GetAllTotal($_SESSION['id_user']);
}else{
	$total = $contact->GetAllOfGroupTotal($id_group,$_SESSION['id_user']);
}

if(isset($_POST["editar"]))
{
	$id = $_POST["id"];

	//Llamar a la vista
	if($contact->GetById($id,$_SESSION['id_user']))
	{
		require_once("views/groups/modContact_view.phtml");
	}
}else if(isset($_POST["aceptar"])){	
	
	$id = $_POST["id"];
	
	$contact->id = $id;
		
	$contact->ParseoDeArray($_POST);

	$result = $contact->Modificar();				
	
	$contacts = $contact->GetAllOfGroup($id_group,$_SESSION['id_user'],$start);
		
	require_once("views/groups/lstContact_view.phtml");	

}else{
	
	$contacts = $contact->GetAllOfGroup($id_group,$_SESSION['id_user'],$start);	

	//Llamar a la vista
	require_once("views/groups/lstContact_view.phtml");	
}
?>