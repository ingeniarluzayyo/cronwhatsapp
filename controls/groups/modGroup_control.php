<?php
define('__SELF_DIR__', dirname(dirname(__FILE__)));
include_once(__SELF_DIR__."../../lib/global.php");

//Llamar a al modelo
require_once("models/Group.class.php");
$group = new Group();

if(isset($_GET['accion']) && $_GET['accion'] == 'borrar' && isset($_GET['id']) && is_numeric($_GET['id']))
{
	$group->GetById($_GET['id'],$_SESSION['id_user']);
	$result = $group->Eliminar();
}

if(isset($_POST["editar"]))
{
	//TODO VALIDAR

	$id = $_POST["id"];

	//Llamar a la vista
	if($group->GetById($id,$_SESSION['id_user']))
	{
		require_once("views/groups/modGroup_view.phtml");
	}
}else if(isset($_POST["aceptar"])){	
	
	$id = $_POST["id"];
	
	$group->id = $id;
		
	$group->id_user = $_SESSION['id_user'];
		
	$group->ParseoDeArray($_POST);

	$result = $group->Modificar();

	$groups = $group->GetAllOfUser($_SESSION['id_user']);

	require_once("views/groups/lstGroup_view.phtml");
	
}else{
	
	$groups = $group->GetAllOfUser($_SESSION['id_user']);	

	//Llamar a la vista
	require_once("views/groups/lstGroup_view.phtml");	
}
?>