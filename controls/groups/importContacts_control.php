<?php
define('__SELF_DIR__', dirname(dirname(__FILE__)));
include_once(__SELF_DIR__."../../lib/global.php");

//Llamar a al modelo
require_once("models/Group.class.php");
require_once("models/Contact.class.php");
$group = new Group();
$contact = new Contact();
	
if(isset($_POST['import']) AND !empty($_FILES['csv']['tmp_name']) AND isset($_POST['id_group']) AND ctype_digit($_POST['id_group']) AND isset($_POST['type']))
{
	if($_POST['type'] == 'custom')
	{			
		if(isset($_POST['prefijo']) && ctype_digit($_POST['prefijo']) && isset($_POST['cant']) && ctype_digit($_POST['cant']) && $_POST['cant'] >= 0)
		{					
			$import_result = $contact->ImportCSV($_FILES['csv']['tmp_name'],$_POST['id_group'],$_POST['prefijo'],$_POST['cant']);	
		}else{
			$import_result = $contact->ImportCSV($_FILES['csv']['tmp_name'],$_POST['id_group']);	
		}
	}else if($_POST['type'] == 'gmail'){
		if(isset($_POST['prefijo']) && ctype_digit($_POST['prefijo']) && isset($_POST['cant']) && ctype_digit($_POST['cant']) && $_POST['cant'] >= 0)
		{					
			$import_result = $contact->ImportCSVGmail($_FILES['csv']['tmp_name'],$_POST['id_group'],$_POST['prefijo'],$_POST['cant']);		
		}else{
			$import_result = $contact->ImportCSVGmail($_FILES['csv']['tmp_name'],$_POST['id_group']);		
		}		
	}	
}

$arr_grupos = $group->GetArrGroupsOfUser($_SESSION['id_user']);

//Llamar a la vista
require_once("views/groups/importContacts_view.phtml");	

?>