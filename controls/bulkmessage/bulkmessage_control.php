<?php
define('__SELF_DIR__', dirname(dirname(__FILE__)));
include_once(__SELF_DIR__."../../lib/global.php");
//Llamar al modelo
require_once("models/Sender.class.php");
require_once("models/Bulk_Messages.class.php");
require_once("models/Campaign.class.php");
require_once("models/Group.class.php");
require_once("models/Contact.class.php");
require_once("models/Usuario.class.php");

//Group
$group = new Group();
$groups = $group->GetAllOfUser($_SESSION['id_user']);


//Manejo de datos
if(isset($_POST['send']) and $_POST['send']=='ok')
{
	$sender = new Sender();
	$user = new Usuario();
	$msj = new Bulk_Messages();
	$campaing = new Campaign();
	$group_post = $_POST['group'];
	$user->GetById($_SESSION['id_user']);
	
	if(!is_numeric($group_post))
	{
		$error = 'Error, must select a group.';
		require_once("views/bulkmessage/bulkmessage_view.phtml");
		exit;
	}

	$contacto = new Contact();
	$contactos = $contacto->GetContactOfGroup($group_post,$_SESSION['id_user']);
	
	if($user->credits < count($contactos))
	{
		$error = 'Your credits are not enough to send '.count($contactos).' messages.';
		require_once("views/bulkmessage/bulkmessage_view.phtml");
		exit;
	}		
	
	//Manejo de archivos y controles de extencion
	$tipo = $_POST['tipo'];
	$user->GetByUsername($_SESSION['username']);
	$user_id =  $user->id;
	$cant_total = count($contactos);
	
	
	switch ($tipo) {
    case 'text':
	    require_once('controls/messages/text.php');
        break;
    case 'imagen':
	 	require_once('controls/messages/image.php');	 
        break;
    case 'audio':
        require_once('controls/messages/audio.php');	
        break;
    case 'video':
        require_once('controls/messages/video.php');  
        break;    
	}
	
	if(!isset($error))
	{
		$campaing->content    = $contenido;
		$campaing->type       = $tipo;
		$campaing->group_id   = $group_post;
		$campaing->cant_total = $cant_total;
		$result = $campaing->Crear();

		if($result)
		{
			$msj->campaign_id = $campaing->LastId();
			foreach($contactos as $contacto)
			{	
				$msj->contact_id = $contacto->id;			
				$result &= $msj->CrearBulk();
		 	}
		}
		
		$result &= $msj->Save();	
		
		if(isset($result) AND $result)
		{
			$user->AddCredits(-count($contactos));
			$user->GetById($_SESSION['id_user']);
		}
	}
}
	
	
	
	require_once("views/bulkmessage/bulkmessage_view.phtml");
?>