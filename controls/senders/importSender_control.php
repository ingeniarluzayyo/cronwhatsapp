<?php
define('__SELF_DIR__', dirname(dirname(__FILE__)));
include_once(__SELF_DIR__."../../lib/global.php");

//Llamar a al modelo
require_once("models/Sender.class.php");

$senders = new Sender();
	
if(isset($_POST['import']) AND !empty($_FILES['csv']['tmp_name']))
{
	$import_result = $senders->ImportCSV($_FILES['csv']['tmp_name']);	
}

//Llamar a la vista
require_once("views/senders/importSenders_view.phtml");	
?>