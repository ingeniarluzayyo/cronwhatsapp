<?php
define('__SELF_DIR__', dirname(dirname(__FILE__)));
include_once(__SELF_DIR__."../../lib/global.php");

//Llamar a al modelo
require_once("lib/api/whatsapp/whatsprot.class.php");
require_once("models/Sender.class.php");
$sender = new Sender();
$value = "";

if(isset($_GET['accion']) && $_GET['accion'] == 'borrar' && isset($_GET['id']) && is_numeric($_GET['id']))
{
	$sender->GetById($_GET['id']);
	$result = $sender->Eliminar();
}

if(isset($_POST["profile_picture"]))
{
	$id = $_POST["id"];
	$sender->GetById($id);
	
	try{
		$wp = new WhatsProt($sender->numero,"WhatsApp",false);
		
		$wp->connect();
		$wp->loginWithPassword($sender->password);
		
		$wp->eventManager()->bind("onGetProfilePicture", "onGetProfilePicture");
		
		$wp->sendGetProfilePicture($sender->numero,false);
	}catch(Exception $e)
	{
		
	}
	
	require_once("views/senders/profile_picture_view.phtml");
	exit();
}

if(isset($_POST["save_profile_picture"]))
{
	$id = $_POST["id"];
	$sender->GetById($id);
	
	try{
		if($_FILES['profile_img']['type'] != "image/jpeg" && $_FILES['profile_img']['type'] != "image/jpg")
		{
			$result = array();
			$result['state'] = false;
			$result['msg'] = "Error the picture type is not jpg,jpeg.";
		}else
		{
			$wp = new WhatsProt($sender->numero,"WhatsApp",false);
			
			$wp->connect();
			$wp->loginWithPassword($sender->password);
			$wp->sendSetProfilePicture($_FILES['profile_img']['tmp_name']);
		}
	}catch(Exception $e)
	{
		$result = array();
		$result['state'] = false;
		$result['msg'] = "Error while upload de profile picture.";
	}
}


if(isset($_POST["editar"]))
{
	$id = $_POST["id"];

	//Llamar a la vista
	if($sender->GetById($id))
	{
		require_once("views/senders/modSender_view.phtml");
	}
}else if(isset($_POST["aceptar"])){	
	
	$id = $_POST["id"];
	
	$sender->id = $id;
		
	$sender->ParseoDeArray($_POST);

	if($sender->IsOnline()){
		$sender->estado = 'Online';
	}else{
		$sender->estado = 'Offline';
	}

	$result = $sender->Modificar();
	
	$senders = $sender->GetAll();

	require_once("views/senders/lstSender_view.phtml");
}else{
	
	if(isset($_POST['deleteoffline']))
	{
		$result = $sender->EliminarOffline();
	}
	
	if(isset($_POST['search']) && $_POST['number'] != "")
	{
		$value = $_POST['number'];
		$senders = $sender->GetByNumber($value);
	}else{
		$value = "";
		$senders = $sender->GetAll();	
	}
	
		
	//Llamar a la vista
	require_once("views/senders/lstSender_view.phtml");	
}

function onGetProfilePicture($from, $target, $type, $data)
{
    if ($type == "preview") {
        $filename = "preview_" . $target . ".jpg";
    } else {
        $filename = $target . ".jpg";
    }
    //define("PICTURES_FOLDER",'img/profile');
    $filename = 'img/profile'."/" . $filename;
    $fp = @fopen($filename, "w");
    if ($fp) {
        fwrite($fp, $data);
        fclose($fp);
    }
}
?>