<?php
  	require_once("models/Packages.class.php");
	require_once("models/Packages_x_User.class.php");
	require_once("models/Usuario.class.php");
	
	$package = new Packages();
	$usuario = new Usuario();
	$packages_x_user = new Packages_x_User();
	
	if(isset($_GET['accion']) && $_GET['accion'] == 'borrar' && isset($_GET['id']) && is_numeric($_GET['id']))
	{
		$packages_x_user->GetById($_GET['id']);
		$result = $packages_x_user->Eliminar();
	}
	
	if(isset($_POST["add"]))
	{
		$id = $_POST["id"];

		//Llamar a la vista
		$packages_x_user->GetById($id);
		
		$usuario->GetById($packages_x_user->user);
		$value = $package->GetCredits($packages_x_user->package);
		
		$result = $usuario->AddCredits($value);
		
		if($result['state'])
		{
			$packages_x_user->Eliminar();
		}	
	}
	
	
	$pxu = $packages_x_user->GetAll();

	require_once("views/packages/PendingCredits.phtml");

?>