<?php
define('__SELF_DIR__', dirname(dirname(__FILE__)));
include_once(__SELF_DIR__."../../lib/global.php");

//Llamar a al modelo
require_once("models/Packages.class.php");
$package = new Packages();
$arr_bg = array('bg-red'=>'bg-red','bg-yellow'=>'bg-yellow','bg-aqua'=>'bg-aqua','bg-blue'=>'bg-blue','bg-green'=>'bg-green','bg-light-blue'=>'bg-light-blue','bg-navy'=>'bg-navy','bg-teal'=>'bg-teal','bg-olive'=>'bg-olive','bg-lime'=>'bg-lime','bg-orange'=>'bg-orange','bg-fuchsia'=>'bg-fuchsia','bg-purple'=>'bg-purple','bg-maroon'=>'bg-maroon','bg-black'=>'bg-black');


if(isset($_GET['accion']) && $_GET['accion'] == 'borrar' && isset($_GET['id']) && is_numeric($_GET['id']))
{
	$package->GetById($_GET['id']);
	$result = $package->Eliminar();
}

if(isset($_POST["editar"]))
{
	$id = $_POST["id"];

	//Llamar a la vista
	if($package->GetById($id))
	{
		require_once("views/packages/modPackage_view.phtml");
	}
}else if(isset($_POST["aceptar"])){	
	
	$id = $_POST["id"];
	
	$package->id = $id;
		
	$package->ParseoDeArray($_POST);

	$result = $package->Modificar();

	$packages = $package->GetAll();
		
	require_once("views/packages/lstPackage_view.phtml");
}else{

	$packages = $package->GetAll();	
	
	//Llamar a la vista
	require_once("views/packages/lstPackage_view.phtml");	
}
?>