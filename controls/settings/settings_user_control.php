<?php

	if(isset($_POST["save_profile_picture"]))
	{
		$id = $_POST["id"];
		
		$nombre = "user_".$id;
		
		$archivo=$_FILES['profile_img']['tmp_name'];
		$nombre_original = $_FILES['profile_img']['name'];
		$tipo_archivo = $_FILES['profile_img']['type'];
		
		if($tipo_archivo != "image/jpeg" && $tipo_archivo != "image/jpg")
		{
			$result = array();
			$result['state'] = false;
			$result['msg'] = "Error the picture type is not jpg,jpeg.";
			require_once("views/settings/settings_user_view.phtml");
			exit();
		}
		
		$tamano_archivo = getimagesize($_FILES['profile_img']['tmp_name']);
		
		if($tamano_archivo[0] != $tamano_archivo[1])
		{
			$result = array();
			$result['state'] = false;
			$result['msg'] = "Error the picture must be square.";
			require_once("views/settings/settings_user_view.phtml");
			exit();
		}
		

		if(ManejoArchivos::guardar_archivo($archivo,$hd_imagenes.'users'.$pait,$nombre,$tipo_archivo,"imagen"))
		{
			$result = array();
			$result['state'] = true;
			$result['msg'] = "The picture was saved correctly.";
		}else
		{
			$result = array();
			$result['state'] = false;
			$result['msg'] = "Error while save the profile picture .";
		}
		
	
	}

	require_once("views/settings/settings_user_view.phtml");
?>