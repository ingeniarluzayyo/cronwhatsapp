<?php
define('__SELF_DIR__', dirname(dirname(__FILE__)));
include_once(__SELF_DIR__."../../lib/global.php");

//Llamar a al modelo
require_once("models/Settings.class.php");
$settings = new Settings();

if(isset($_POST["aceptar"])){	
	
	$id = $_POST["id"];
	
	$settings->id = $id;
		
	$settings->ParseoDeArray($_POST);

	$result = $settings->Modificar();	
}

//Llamar a la vista
if($settings->GetById(1))
{
		require_once("views/settings/settings_view.phtml");
}

?>