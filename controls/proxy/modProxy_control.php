<?php
define('__SELF_DIR__', dirname(dirname(__FILE__)));
include_once(__SELF_DIR__."../../lib/global.php");

//Llamar a al modelo
require_once("models/Proxy.class.php");

$proxy = new Proxy();
$arr_tipos = $proxy->GetArrTipos();
$arr_auth = $proxy->arr_auth;

if(isset($_GET['accion']) && $_GET['accion'] == 'borrar' && isset($_GET['id']) && is_numeric($_GET['id']))
{
	$proxy->GetById($_GET['id']);
	$result = $proxy->Eliminar();
}

if(isset($_POST["editar"]))
{
	$id = $_POST["id"];
		
	//Llamar a la vista
	if($proxy->GetById($id))
	{
		require_once("views/proxy/modProxy_view.phtml");
		exit();
	}
	
}else if(isset($_POST["aceptar"])){
	$proxy->ParseoDeArray($_POST);
	
	$result = $proxy->Validar();

	if($result['state']){
		$url = "http://google.com";
		$proxy->estado = $proxy->isOnline($url);
			
		$result = $proxy->Modificar();	
	}
}

if(isset($_POST["conexion"]))
{
	$id = $_POST["id"];
	$proxy->GetById($id);
	
	$url = "http://google.com";
	$proxy->estado = $proxy->isOnline($url);

	$result = $proxy->UpdateStatus();
}


$proxys = $proxy->GetAll();
require_once("views/proxy/lstProxy_view.phtml");	




?>