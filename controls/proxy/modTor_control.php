<?php
define('__SELF_DIR__', dirname(dirname(__FILE__)));
include_once(__SELF_DIR__."../../lib/global.php");

//Llamar a al modelo
require_once("models/Tor.class.php");
require_once("models/CrawlerPersonal.class.php");

$tor = new Tor();
$tor->GetConfig();

if(isset($_POST["accion"]) && $_POST["accion"] = 'starttor')
{
	//Llamar a la vista
	Tor::startTor();

}else if((isset($_POST["accion"]) && $_POST["accion"] = 'starttor')){
	//Llamar a la vista
	Tor::stopTor();
}

if(isset($_POST["editar"]))
{
	//Llamar a la vista
	if($tor->GetConfig())
	{
		require_once("views/proxy/modTor_view.phtml");
		exit();
	}

}else if(isset($_POST["aceptar"])){
	
	$tor->ParseoDeArray($_POST);
	
	$result = $tor->Validar();

	if($result['state']){
		$result = $tor->Modificar();
	}	
}

$url = $settings->url_check;

$tor->estado = $tor->isOnline($url);
$tor->UpdateStatus();

if($tor->estado == 'Online'){
	$conexion = '<span style="color:#00A65A;">Online</span>';
}else{
	$conexion = '<span style="color:#F56954;">Offline</span>';
}

require_once("views/proxy/modTor_view.phtml");




?>