<?php
define('__SELF_DIR__', dirname(dirname(__FILE__)));
include_once(__SELF_DIR__."../../lib/global.php");

//Llamar a al modelo
require_once("models/Proxy.class.php");

$proxy = new Proxy();
$arr_tipos = $proxy->GetArrTipos();
$arr_auth = $proxy->arr_auth;

if(isset($_POST["aceptar"]))
{
	$proxy->ParseoDeArray($_POST);	
	
	$result = $proxy->Validar();

	if($result['state']){
		$url = "http://www.google.com";
		$proxy->estado = $proxy->isOnline($url);

		$result = $proxy->Crear();
	}
}

//Llamar a la vista
require_once("views/proxy/newProxy_view.phtml");

?>