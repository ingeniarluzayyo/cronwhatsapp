<?php
define('__SELF_DIR__', dirname(dirname(__FILE__)));
include_once(__SELF_DIR__."../../lib/global.php");
//Llamar al modelo
require_once("lib/api/whatsapp/whatsprot.class.php");
require_once("models/HashMedia.class.php");
require_once("models/Sender.class.php");
require_once("models/Message.class.php");
require_once("models/Single_Messages.class.php");
require_once("models/Credits.class.php");
require_once("models/Contact.class.php");


//Manejo de datos
if(isset($_POST['send']) and $_POST['send']=='ok')
{
	$sender = new Sender();
	
	//Verificaciones
	if(empty($_POST['contacto']))
	{
		$error = 'Error, must enter a mobile number.';
		require_once("views/singlemessage/singlemessage_view.phtml");
		exit;
	}
	
	if(!ctype_digit($_POST['contacto']))
	{
		$error = 'Error, the mobile number only accepts numeric characters.';
		require_once("views/singlemessage/singlemessage_view.phtml");
		exit;
	}
	
	if($user->credits < 1)
	{
		$error = 'Your credits are not enough to send a message.';
		require_once("views/singlemessage/singlemessage_view.phtml");
		exit;
	}
	
	$senderCheck = $sender->HaveEnougthSenders($_SESSION['id_user']);

	if(!$senderCheck){
		$error = 'There are no senders available to send a message, plase try it later. ';
		require_once("views/singlemessage/singlemessage_view.phtml");
		exit;
	}
	
	$numero = $_POST['contacto'];	
	$contacto = new Contact();
	$contacto->number = $numero;
	
	//Manejo de archivos y controles de extencion
	$tipo = $_POST['tipo'];

	switch ($tipo) {
    case 'text':
	    require_once('controls/messages/text.php');
        break;
    case 'imagen':
	 	require_once('controls/messages/image.php');	 
        break;
    case 'audio':
        require_once('controls/messages/audio.php');	
        break;
    case 'video':
        require_once('controls/messages/video.php');  
        break;    
	}
	
	if(!isset($error)){
		//Devuelve el id del sender con el que envio
		$msj = new Single_Messages();
		$sender_id = $msj->Send($_SESSION['id_user'],$contacto,$tipo,$contenido,TRUE);		
		
		if(isset($sender_id) && $sender_id > 0){
			$credits = new Credits();
			$credits->DecreaseCredits($_SESSION['id_user'],1);
			$user->GetById($_SESSION['id_user']);
			
			$msg = new Single_Messages();
			$msg->number = $contacto->number;
			$msg->content = $contenido;
			$msg->type = $tipo;
			$msg->user = $_SESSION['id_user'];
			$msg->sender_id = $sender_id;
			$msg->Crear();	
			
			$result = true;
		}else{
			$error = 'An error happend while sending the message.';
		}
		
	}
}

//Llamar a la vista
require_once("views/singlemessage/singlemessage_view.phtml");
?>