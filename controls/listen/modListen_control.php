<?php
define('__SELF_DIR__', dirname(dirname(__FILE__)));
include_once(__SELF_DIR__."../../lib/global.php");

//Llamar a al modelo
require_once("models/ListenSent.class.php");
require_once("models/Sender.class.php");
$listen = new ListenSent();
$sender = new Sender();

if(isset($_GET['accion']) && $_GET['accion'] == 'borrar' && isset($_GET['id']) && is_numeric($_GET['id']))
{
	$listen->GetById($_GET['id']);
	$result = $listen->Eliminar();
}


if(isset($_POST['search']) && $_POST['fecha_escucha'] != "")
{	
	$value = $_POST['fecha_escucha'];	
	$listens = $listen->Search($_POST['fecha_escucha']);
}else{
	$value = "";	
	$listens = $listen->GetAll();
}
	

//Llamar a la vista
require_once("views/listen/lstListen_view.phtml");	

?>