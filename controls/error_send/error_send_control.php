<?php
define('__SELF_DIR__', dirname(dirname(__FILE__)));
include_once(__SELF_DIR__."../../lib/global.php");

//Llamar a al modelo
require_once("models/Usuario.class.php");
require_once("models/ErrorLog.class.php");
require_once("models/Contact.class.php");
//require_once("models/Message_to_send.class.php");
$message = new ErrorLog();
//$msg_to_send = new Message_to_send();	
	

if(isset($_POST['accion']) && $_POST['accion'] == "resend")	
{	
	$logs_to_resend = array();	
	for($i=1;$i<$_POST['total'];$i++)
	{
		$id_log = (isset($_POST['id_'.$i]) && is_numeric($_POST['id_'.$i]))?$_POST['id_'.$i]:0;
		if(is_numeric($id_log) && $id_log != 0)
		{
			$log = new ErrorLog();
			$log->GetById($id_log);
			array_push($logs_to_resend,$log);
		}
		//$msg_to_send->ResendErrors($logs_to_resend);
	}
}

if(isset($_POST['accion']) && $_POST['accion'] == "delete")	
{			
	for($i=1;$i<=$_POST['total'];$i++)
	{		
		$id_log = (is_numeric($_POST['id_'.$i]))?$_POST['id_'.$i]:0;		
		if($id_log != 0)
		{
			$log = new ErrorLog();
			$log->GetById($id_log);
			$log->Eliminar();
		}		
	}
}
if(isset($_GET['accion']) && $_GET['accion'] == "resendall")	
{		
	$messages = $message->GetAllToResend();		
	//$msg_to_send->ResendErrors($messages);		
}

if(isset($_GET['accion']) && $_GET['accion'] == "deleteall")	
{
	$messages = $message->GetAll();	
	$message->DeleteAll($messages);
}
	
$messages = $message->GetAll();	

//Llamar a la vista
require_once("views/error_send/lstErrorSend_view.phtml");	

?>