-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-08-2015 a las 20:54:54
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `cronwa`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bulk_messages`
--

CREATE TABLE IF NOT EXISTS `bulk_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) NOT NULL,
  `contact_id` bigint(20) NOT NULL,
  `date_sent` datetime NOT NULL,
  `sender_id` int(11) NOT NULL,
  `sent` int(11) NOT NULL,
  `clean` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `bulk_messages`
--

INSERT INTO `bulk_messages` (`id`, `campaign_id`, `contact_id`, `date_sent`, `sender_id`, `sent`, `clean`) VALUES
(1, 1, 2, '2015-08-10 00:00:00', 2, 1, 1),
(2, 2, 2, '2015-08-13 21:42:20', 3, 1, 0),
(3, 3, 2, '2015-08-13 17:40:11', 3, 1, 0),
(4, 3, 3, '2015-08-13 17:40:11', 3, 1, 0),
(5, 4, 4, '2015-08-13 17:48:53', 3, 1, 0),
(6, 4, 5, '2015-08-13 17:48:53', 3, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `campaign`
--

CREATE TABLE IF NOT EXISTS `campaign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `date` datetime NOT NULL,
  `type` text NOT NULL,
  `group_id` int(11) NOT NULL,
  `status` text NOT NULL,
  `cant_total` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `campaign`
--

INSERT INTO `campaign` (`id`, `content`, `date`, `type`, `group_id`, `status`, `cant_total`) VALUES
(1, 'test', '2015-08-10 00:00:00', 'text', 1, 'paused', 100),
(2, 'test', '2015-08-13 16:19:15', 'text', 3, 'in progress', 1),
(3, 'fjdsfefes', '2015-08-13 16:46:41', 'text', 3, 'waiting', 2),
(4, 'fdgdrbrdgrdr', '2015-08-13 17:48:12', 'text', 4, 'finish', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_group` int(11) NOT NULL,
  `name` text NOT NULL,
  `number` bigint(20) NOT NULL,
  `whatsapp_install` varchar(30) NOT NULL,
  `state` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `contacts`
--

INSERT INTO `contacts` (`id`, `id_group`, `name`, `number`, `whatsapp_install`, `state`) VALUES
(1, 1, 'luciano', 5491121692730, 'Yes', 1),
(2, 3, 'laklsd', 4543535435, 'No', 1),
(3, 3, 'test 1', 5491168201786, 'Yes', 1),
(4, 4, 'test', 5491168201786, 'Yes', 1),
(5, 4, 'test2', 5491121692730, 'Yes', 1);

--
-- Disparadores `contacts`
--
DROP TRIGGER IF EXISTS `trigger_restar_contacto`;
DELIMITER //
CREATE TRIGGER `trigger_restar_contacto` AFTER DELETE ON `contacts`
 FOR EACH ROW BEGIN 
 
 	UPDATE groups SET cant=cant-1 WHERE id = OLD.id_group; 
    
 END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_sumar_contacto`;
DELIMITER //
CREATE TRIGGER `trigger_sumar_contacto` AFTER INSERT ON `contacts`
 FOR EACH ROW BEGIN

	UPDATE groups SET cant=cant+1 WHERE id = NEW.id_group;

END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `error_send`
--

CREATE TABLE IF NOT EXISTS `error_send` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `contact` int(11) NOT NULL,
  `contenido` text NOT NULL,
  `type` text NOT NULL,
  `fecha` datetime NOT NULL,
  `error_type` text NOT NULL,
  `sender` text NOT NULL,
  `exception` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `id_user` int(11) NOT NULL,
  `cant` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `groups`
--

INSERT INTO `groups` (`id`, `name`, `id_user`, `cant`) VALUES
(1, 'test', 2, 1),
(2, '5491121692730', 2, 0),
(3, 'test_2', 2, 2),
(4, 'group', 3, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hash_media`
--

CREATE TABLE IF NOT EXISTS `hash_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hash` varchar(65) NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hash` (`hash`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `hash_media`
--

INSERT INTO `hash_media` (`id`, `hash`, `fecha`) VALUES
(1, 'Psa06QmTvmTPE8yiIVghgvLGlhip2AIb76JzgUA1NWo=', '2015-08-13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `listens_sent`
--

CREATE TABLE IF NOT EXISTS `listens_sent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contenido` text NOT NULL,
  `numero` bigint(20) NOT NULL,
  `sender` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `fecha_escucha` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `listens_sent`
--

INSERT INTO `listens_sent` (`id`, `contenido`, `numero`, `sender`, `fecha`, `fecha_escucha`) VALUES
(1, 'Jebshshf', 5491168201786, 3, '2015-08-14 15:48:48', '2015-08-14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `packages`
--

CREATE TABLE IF NOT EXISTS `packages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `credits` int(11) NOT NULL,
  `amount` text NOT NULL,
  `bg_color` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `packages`
--

INSERT INTO `packages` (`id`, `credits`, `amount`, `bg_color`) VALUES
(1, 10000, '1000', 'bg-red');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `packages_x_user`
--

CREATE TABLE IF NOT EXISTS `packages_x_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime NOT NULL,
  `package` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `senders`
--

CREATE TABLE IF NOT EXISTS `senders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero` text NOT NULL,
  `password` text NOT NULL,
  `estado` text NOT NULL,
  `enviados` int(11) NOT NULL,
  `ultima_actualizacion` datetime NOT NULL,
  `assigned_user` int(11) NOT NULL,
  `habilitado` varchar(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `senders`
--

INSERT INTO `senders` (`id`, `numero`, `password`, `estado`, `enviados`, `ultima_actualizacion`, `assigned_user`, `habilitado`) VALUES
(3, '5491141968110', 'T3ixM8FZofmHLt0aEaoqOyrLnAQ=', 'Online', 24, '2015-08-14 15:07:15', 0, '1'),
(4, '5491169508459', 'DnPBDCzIfFpbh0hjtpM5kFRkHpM=', 'Offline', 0, '2015-08-14 15:07:01', 0, '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_name` text NOT NULL,
  `currency` text NOT NULL,
  `max_msg_sender` int(11) NOT NULL,
  `timezone` text NOT NULL,
  `whatsapp_version` text NOT NULL,
  `hora_inicio` text NOT NULL,
  `hora_fin` text NOT NULL,
  `sleep_time_between_msg` int(11) NOT NULL,
  `is_online_time` text NOT NULL,
  `listen_time` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `settings`
--

INSERT INTO `settings` (`id`, `site_name`, `currency`, `max_msg_sender`, `timezone`, `whatsapp_version`, `hora_inicio`, `hora_fin`, `sleep_time_between_msg`, `is_online_time`, `listen_time`) VALUES
(1, 'CronWhatsApp', '$', 100, 'America/Argentina/Buenos_Aires', '2.11.16', '8:00', '21:00', 1, '1:00', '6:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `single_messages`
--

CREATE TABLE IF NOT EXISTS `single_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` bigint(20) NOT NULL,
  `content` text NOT NULL,
  `type` text NOT NULL,
  `user` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `sender_id` int(11) NOT NULL,
  `clean` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Volcado de datos para la tabla `single_messages`
--

INSERT INTO `single_messages` (`id`, `number`, `content`, `type`, `user`, `date`, `sender_id`, `clean`) VALUES
(1, 5491121682730, 'test asdasda', 'text', 2, '2015-08-11 00:00:00', 2, 1),
(3, 5491168201786, 'test test', 'text', 2, '2015-08-13 15:36:54', 3, 0),
(4, 5491168201786, 'test test', 'text', 2, '2015-08-13 15:39:09', 3, 0),
(5, 5491168201786, 'test test', 'text', 2, '2015-08-13 15:40:10', 3, 0),
(6, 5491168201786, 'test test', 'text', 2, '2015-08-13 15:40:51', 3, 0),
(7, 5491168201786, 'test test', 'text', 2, '2015-08-13 15:43:44', 3, 0),
(8, 5491168201786, 'C:\\wamp\\www\\cronwhatsapp\\message_record\\imagen\\cf3600028b6d8e3851d1fc217259495e.jpg;holass', 'image', 2, '2015-08-13 15:58:25', 3, 0),
(9, 5491168201786, 'C:\\wamp\\www\\cronwhatsapp\\message_record\\imagen\\137eb8bc0c047673bf578f1a942e23df.jpg;holass', 'image', 2, '2015-08-13 15:58:47', 3, 0),
(10, 5491168201786, 'C:\\wamp\\www\\cronwhatsapp\\message_record\\video\\ad5e7f2aecec3d52b15b826e403e86c1.mp4;', 'video', 2, '2015-08-13 15:59:39', 3, 0),
(11, 5491168201786, 'C:\\wamp\\www\\cronwhatsapp\\message_record\\audio\\7ba846418b15c2b62a0cb692ef4ecebc.mp3', 'audio', 2, '2015-08-13 16:11:26', 3, 0),
(12, 5491168201786, 'C:\\wamp\\www\\cronwhatsapp\\message_record\\audio\\987006b9e1534e55d409a7ee3dd0fde7.mp3', 'audio', 2, '2015-08-13 16:12:13', 3, 0),
(13, 5491168201786, 'C:\\wamp\\www\\cronwhatsapp\\message_record\\video\\3b846e2f890e7f4d88359a359a9a8be9.mp4;', 'video', 2, '2015-08-13 16:14:44', 3, 0),
(14, 5491168201786, 'C:\\wamp\\www\\cronwhatsapp\\message_record\\audio\\aa9acb20f8fca5b46d4412a782029fba.mp3', 'audio', 2, '2015-08-13 16:17:23', 3, 0),
(15, 5491168201786, 'C:\\wamp\\www\\cronwhatsapp\\message_record\\audio\\ea1e6193873672fbf1cf868be5fd6a1b.mp3', 'audio', 2, '2015-08-13 16:18:12', 3, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(512) NOT NULL,
  `email` text NOT NULL,
  `telefono` text NOT NULL,
  `user_type` varchar(300) NOT NULL,
  `credits` int(11) NOT NULL DEFAULT '0',
  `habilitado` varchar(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `username`, `password`, `email`, `telefono`, `user_type`, `credits`, `habilitado`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '', '', 'admin', 0, '1'),
(2, 'cliente', '4983a0ab83ed86e0e7213c8783940193', '', '', '', 65710, '1'),
(3, 'test', '098f6bcd4621d373cade4e832627b4f6', 'test', '', '', 998, '1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
